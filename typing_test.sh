tested=0
passed=0

function typing_test_dir() {
  for file in "$1"/*.purs
  do
    ./ppurs --type-only "$file"
    exit_code=$?
    ((tested = tested + 1))
    if [ $exit_code -eq $2 ]
    then
      ((passed = passed + 1))
      echo "$file: test passed."
    else
      echo "$file: test failed."
    fi
  done
}

typing_test_dir "./tests-10-nov/typing/bad" 1
typing_test_dir "./tests-10-nov/typing/good" 0
typing_test_dir "./tests-10-nov/exec" 0

echo "$tested files tested, $passed passed."