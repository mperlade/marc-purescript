tested=0
passed=0

for file in "./tests-10-nov/exec"/*.purs
do
  ./ppurs "$file"
  exit_code=$?
  ((tested = tested + 1))
  expected_output_path="${file%.purs}.out"
  base_name=$(basename -- "$file")
  assembly_name="${base_name%.purs}.s"
  if [ $exit_code -eq 0 ]
  then
    gcc -g -no-pie "$assembly_name"
    output="$(./a.out)"
    expected_output="$(cat "$expected_output_path")"
    if [ "$output" = "$expected_output" ]
    then
      ((passed = passed + 1))
      echo "$file: test passed."
    else
      echo "$file: test failed."
    fi
    rm "./a.out"
    rm "$assembly_name"
  else
    echo "$file: test failed (compiler error)."
  fi
done

echo "$tested files tested, $passed passed."