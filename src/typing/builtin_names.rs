//Primitive types
pub const UNIT_IDENTIFIER: &str = "Unit";
pub const BOOLEAN_IDENTIFIER: &str = "Boolean";
pub const INT_IDENTIFIER: &str = "Int";
pub const STRING_IDENTIFIER: &str = "String";
pub const EFFECT_IDENTIFIER: &str = "Effect";

//Built-in functions
pub const NOT_IDENTIFIER: &str = "not";
pub const MOD_IDENTIFIER: &str = "mod";
pub const LOG_IDENTIFIER: &str = "log";
pub const PURE_IDENTIFIER: &str = "pure";
pub const SHOW_IDENTIFIER: &str = "show";
pub const INT_TO_STRING_IDENTIFIER: &str = "@int_to_string";
pub const UNIT_CONST_IDENTIFIER: &str = "unit";

pub const SHOW_CLASS_IDENTIFIER: &str = "Show";

pub const MAIN_IDENTIFIER: &str = "main";

pub const PATTERN_DO_NOT_CARE_IDENTIFIER: &str = "_";