use crate::parser::ast::{Definition, Instance, InstanceDeclaration, TypeExpression};
use crate::typing;
use crate::typing::environment::{
    EnvironmentInstance, EnvironmentInstanceSchema, GlobalEnvironment, LocalEnvironment,
};
use crate::typing::type_error::TypeError;
use crate::typing::typed_file::{Type, TypeVariableIdentifier};
use crate::typing::{definition, unification};
use std::collections::hash_map::Entry;
use std::collections::{HashMap, HashSet};
use std::iter;
use std::rc::Rc;

fn extract_type_variables(
    type_expression: &TypeExpression,
    encountered: &mut HashMap<String, TypeVariableIdentifier>,
) {
    match type_expression {
        TypeExpression::TypeVariable(_, variable) => {
            let identifier = encountered.len();
            match encountered.entry(variable.clone()) {
                Entry::Occupied(_) => {}
                Entry::Vacant(e) => {
                    e.insert(identifier);
                }
            }
        }
        TypeExpression::DataType(_, _, arguments) => {
            for argument in arguments {
                extract_type_variables(argument, encountered);
            }
        }
    }
}

fn extract_instance_type_variables(
    instance: &Instance,
    encountered: &mut HashMap<String, TypeVariableIdentifier>,
) {
    for argument in instance.arguments.iter() {
        extract_type_variables(argument, encountered);
    }
}

fn replace_type_variables(r#type: &Type, replacements: &[Type]) -> Type {
    match r#type {
        Type::TypeVariable(identifier, _) => replacements[*identifier].clone(),
        Type::Effect(r#type) => Type::Effect(Rc::new(replace_type_variables(r#type, replacements))),
        Type::DataType(identifier, name, types) => Type::DataType(
            *identifier,
            name.clone(),
            Rc::from(
                types
                    .iter()
                    .map(|r#type| replace_type_variables(r#type, replacements))
                    .collect::<Vec<Type>>(),
            ),
        ),
        _ => r#type.clone(),
    }
}

pub fn type_instance_declaration(
    global_environment: &mut GlobalEnvironment,
    instance_declaration: InstanceDeclaration,
) -> Result<(), TypeError> {
    let mut type_variables = HashMap::new();
    for instance in instance_declaration.constraints.iter() {
        extract_instance_type_variables(instance, &mut type_variables);
    }
    extract_instance_type_variables(&instance_declaration.instance, &mut type_variables);

    let local_environment = LocalEnvironment::with_type_variables(&type_variables);

    let constraints = instance_declaration
        .constraints
        .into_iter()
        .map(|instance| {
            typing::well_formed_instance(global_environment, &local_environment, instance)
        })
        .collect::<Result<Vec<EnvironmentInstance>, TypeError>>()?;

    let instance = typing::well_formed_instance(
        global_environment,
        &local_environment,
        instance_declaration.instance,
    )?;

    //First check that the types of the instance cannot be unified with the types of any other instance
    let class = &global_environment.classes[instance.class];
    'next_schema: for schema in class.instance_schemas.iter() {
        let mut my_replacements = HashMap::new();
        let mut other_replacements = HashMap::new();
        let mut unified = HashMap::new();
        for (my_argument, other_argument) in instance.arguments.iter().zip(schema.arguments.iter())
        {
            let my_argument = unification::temp_type_variables(
                global_environment,
                &mut my_replacements,
                my_argument,
            );
            let other_argument = unification::temp_type_variables(
                global_environment,
                &mut other_replacements,
                other_argument,
            );
            if !unification::unify_types_recursive(&mut unified, &my_argument, &other_argument) {
                continue 'next_schema;
            }
        }

        //We successfully unified all variables: BAD
        return Err(TypeError::InstanceSchemasCouldOverlap(
            instance_declaration.location,
        ));
    }

    //Add the instance schema (before, in case there are recursive calls)
    global_environment.classes[instance.class]
        .instance_schemas
        .push(EnvironmentInstanceSchema {
            type_variable_count: local_environment.variables.len(),
            constraints: constraints.clone(),
            arguments: instance.arguments.clone(),
        });

    //Now type the functions
    let local_environment = local_environment.set_instances(&constraints);

    let mut already_processed = HashSet::new();

    let mut definition_iter = instance_declaration.definitions.into_iter().peekable();
    while let Some(definition) = definition_iter.next() {
        let location = definition.location;
        let identifier = definition.identifier.clone();
        let definitions: Vec<Definition> = iter::once(definition)
            .chain(iter::from_fn(|| {
                definition_iter.next_if(|definition| definition.identifier == identifier)
            }))
            .collect();

        //Check that we are defining a function that exists in the correct class
        let function_identifier =
            if let Some(function) = global_environment.functions.get(&identifier) {
                if function.class != instance.class {
                    return Err(TypeError::InstanceFunctionNotDeclaredInClass(
                        location, identifier,
                    ));
                }
                function.identifier
            } else {
                return Err(TypeError::UnrecognizedIdentifier(location, identifier));
            };

        //Check that the definitions are contiguous
        if !already_processed.insert(function_identifier) {
            return Err(TypeError::InstanceFunctionMultipleDefinitions(
                location, identifier,
            ));
        }

        //Compute expected type by replacing typing variables in class definition
        let expected_types = &global_environment.output.functions[function_identifier].types;
        let expected_types: Vec<Type> = expected_types
            .iter()
            .map(|r#type| replace_type_variables(r#type, &instance.arguments))
            .collect();

        //Type the definitions
        let body = definition::type_function_body(
            global_environment,
            &local_environment,
            location,
            &expected_types,
            definitions,
        )?;

        //Add the body
        global_environment.output.functions[function_identifier]
            .bodies
            .push(body);
    }

    let class = &mut global_environment.classes[instance.class];

    //Check that all the functions have been processed
    if class.functions.len() != already_processed.len() {
        return Err(TypeError::InstanceIncomplete(instance_declaration.location));
    }

    Ok(())
}
