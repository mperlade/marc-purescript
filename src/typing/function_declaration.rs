use crate::parser::ast::{Definition, TypeDeclaration};
use crate::typing;
use crate::typing::environment::{
    EnvironmentClass, EnvironmentFunction, EnvironmentInstance, EnvironmentInstanceSchema,
    GlobalEnvironment, LocalEnvironment,
};
use crate::typing::type_error::TypeError;
use crate::typing::typed_file::{Type, TypeVariableIdentifier, TypedFunction};
use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::rc::Rc;
use crate::typing::definition;

pub fn type_function_declaration(
    global_environment: &mut GlobalEnvironment,
    type_declaration: TypeDeclaration,
    definitions: Vec<Definition>,
) -> Result<(), TypeError> {
    let mut type_variables: HashMap<String, TypeVariableIdentifier> = HashMap::new();
    let mut type_variable_types = vec![];
    for identifier in type_declaration.type_variables.into_iter() {
        let index = type_variables.len();
        if let Entry::Vacant(e) = type_variables.entry(identifier.clone()) {
            e.insert(index);
            type_variable_types.push(Type::TypeVariable(index, Rc::from(identifier)))
        }
    }

    let local_environment = LocalEnvironment::with_type_variables(&type_variables);

    //Check that instances are well formed
    let constraints = type_declaration
        .constraints
        .into_iter()
        .map(|instance| {
            typing::well_formed_instance(global_environment, &local_environment, instance)
        })
        .collect::<Result<Vec<EnvironmentInstance>, TypeError>>()?;

    //Check that the signature is well formed
    let types = type_declaration
        .types
        .into_iter()
        .map(|type_expression| {
            typing::well_formed_type(global_environment, &local_environment, type_expression)
        })
        .collect::<Result<Vec<Type>, TypeError>>()?;

    //Create a dummy class with a dummy instance and the add the function
    let class_identifier = global_environment.classes.len();

    let function_identifier = global_environment.output.functions.len();
    global_environment.output.functions.push(TypedFunction {
        types: types.clone(),
        bodies: vec![],
    });

    let environment_function = EnvironmentFunction {
        class: class_identifier,
        identifier: function_identifier,
    };

    if global_environment
        .functions
        .insert(type_declaration.identifier.clone(), environment_function)
        .is_some()
    {
        return Err(TypeError::FunctionDeclaredTwice(
            type_declaration.location,
            type_declaration.identifier,
        ));
    }

    global_environment.classes.push(EnvironmentClass {
        function_dummy_class: true,
        arity: type_variables.len(),
        functions: [function_identifier].into_iter().collect(),
        instance_schemas: vec![EnvironmentInstanceSchema {
            type_variable_count: type_variables.len(),
            constraints: constraints.clone(),
            arguments: type_variable_types,
        }],
    });

    //Type the function body
    let local_environment = local_environment.set_instances(&constraints);

    let body = definition::type_function_body(
        global_environment,
        &local_environment,
        type_declaration.location,
        &types,
        definitions,
    )?;

    global_environment.output.functions[function_identifier].bodies.push(body);

    Ok(())
}
