use std::rc::Rc;
use crate::parser::ast::BinaryOperation;
use crate::typing::environment::TempVariableIdentifier;

pub type DataIdentifier = usize;
pub type VariantIndex = usize;
pub type TypeVariableIdentifier = usize;
pub type InstanceIdentifier = usize;
pub type InstanceVariableIdentifier = usize;
pub type VariableIdentifier = usize;
pub type FunctionIdentifier = usize;

#[derive(Debug)]
pub struct TypedFile {
    pub data: Vec<TypedData>,
    pub functions: Vec<TypedFunction>,
    pub main: FunctionIdentifier,
}

#[derive(Debug)]
pub struct TypedData {
    pub name: Rc<str>,
    pub arity: TypeVariableIdentifier,
    pub variants: Vec<Vec<Type>>,
}

#[derive(Debug, Clone)]
pub enum Type {
    TempVariable(TempVariableIdentifier),
    TypeVariable(TypeVariableIdentifier, Rc<str>),
    Unit,
    Boolean,
    Int,
    String,
    Effect(Rc<Type>),
    DataType(DataIdentifier, Rc<str>, Rc<[Type]>),
}

#[derive(Debug)]
pub struct FunctionBody {
    pub instance_variable_count: InstanceVariableIdentifier,
    pub arguments: Vec<VariableIdentifier>,
    pub expression: TypedExpression,
}

#[derive(Debug)]
pub struct TypedFunction {
    pub types: Vec<Type>,
    pub bodies: Vec<FunctionBody>, //Indices: InstanceIdentifier
}

#[derive(Debug)]
pub enum TypedPattern {
    Constant(Type, Constant),
    Variable(Type, VariableIdentifier),
    Constructor(Type, DataIdentifier, VariantIndex, Vec<TypedPattern>),
}

impl TypedPattern {
    pub fn get_type(&self) -> &Type {
        match self {
            TypedPattern::Constant(r#type, _)
            | TypedPattern::Variable(r#type, _)
            | TypedPattern::Constructor(r#type, _, _, _)
            => r#type,
        }
    }

    pub fn get_type_mut(&mut self) -> &mut Type {
        match self {
            TypedPattern::Constant(r#type, _)
            | TypedPattern::Variable(r#type, _)
            | TypedPattern::Constructor(r#type, _, _, _)
            => r#type,
        }
    }
}

#[derive(Debug)]
pub enum InstanceResolution {
    InstanceVariable(InstanceVariableIdentifier),
    InstanceSchema(InstanceIdentifier, Vec<InstanceResolution>),
}

#[derive(Debug)]
pub enum TypedExpression {
    Constant(Type, Constant),
    LocalVariable(Type, VariableIdentifier),
    Constructor(Type, DataIdentifier, VariantIndex, Vec<TypedExpression>),
    UnaryMinus(Type, Box<TypedExpression>),
    BinaryOperation(Type, Box<TypedExpression>, BinaryOperation, Box<TypedExpression>),
    Application(Type, InstanceResolution, FunctionIdentifier, Vec<TypedExpression>),
    IfThenElse(Type, Box<TypedExpression>, Box<TypedExpression>, Box<TypedExpression>),
    Do(Type, Vec<TypedExpression>),
    Let(Type, Vec<(VariableIdentifier, TypedExpression)>, Box<TypedExpression>),
    Case(Type, Box<TypedExpression>, Vec<(TypedPattern, TypedExpression)>),

    //Built-in functions
    Not(Type, Box<TypedExpression>),
    Mod(Type, Box<TypedExpression>, Box<TypedExpression>),
    Log(Type, Box<TypedExpression>),
    Pure(Type, Box<TypedExpression>),
    IntToString(Type, Box<TypedExpression>),
    Unit(Type),
}

impl TypedExpression {
    pub fn get_type(&self) -> &Type {
        match self {
            TypedExpression::Constant(r#type, _)
            | TypedExpression::LocalVariable(r#type, _)
            | TypedExpression::Constructor(r#type, _, _, _)
            | TypedExpression::UnaryMinus(r#type, _)
            | TypedExpression::BinaryOperation(r#type, _, _, _)
            | TypedExpression::Application(r#type, _, _, _)
            | TypedExpression::IfThenElse(r#type, _, _, _)
            | TypedExpression::Do(r#type, _)
            | TypedExpression::Let(r#type, _, _)
            | TypedExpression::Case(r#type, _, _)
            | TypedExpression::Not(r#type, _)
            | TypedExpression::Mod(r#type, _, _)
            | TypedExpression::Log(r#type, _)
            | TypedExpression::Pure(r#type, _)
            | TypedExpression::IntToString(r#type, _)
            | TypedExpression::Unit(r#type)
            => r#type
        }
    }

    pub fn get_type_mut(&mut self) -> &mut Type {
        match self {
            TypedExpression::Constant(r#type, _)
            | TypedExpression::LocalVariable(r#type, _)
            | TypedExpression::Constructor(r#type, _, _, _)
            | TypedExpression::UnaryMinus(r#type, _)
            | TypedExpression::BinaryOperation(r#type, _, _, _)
            | TypedExpression::Application(r#type, _, _, _)
            | TypedExpression::IfThenElse(r#type, _, _, _)
            | TypedExpression::Do(r#type, _)
            | TypedExpression::Let(r#type, _, _)
            | TypedExpression::Case(r#type, _, _)
            | TypedExpression::Not(r#type, _)
            | TypedExpression::Mod(r#type, _, _)
            | TypedExpression::Log(r#type, _)
            | TypedExpression::Pure(r#type, _)
            | TypedExpression::IntToString(r#type, _)
            | TypedExpression::Unit(r#type)
            => r#type
        }
    }
}

#[derive(Debug, Eq, PartialEq)]
pub enum Constant {
    Integer(i32),
    String(String),
    Boolean(bool),
}
