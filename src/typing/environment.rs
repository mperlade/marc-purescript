use crate::typing::typed_file::{DataIdentifier, FunctionIdentifier, Type, TypeVariableIdentifier, TypedFile, VariableIdentifier, VariantIndex};
use immutable_chunkmap::map::MapM;
use std::cell::Cell;
use std::collections::{HashMap, HashSet};
use crate::typing::builtin_names;

pub type TempVariableIdentifier = usize;
pub type ClassIdentifier = usize;

#[derive(Debug, Clone)]
pub struct EnvironmentInstance {
    pub class: ClassIdentifier,
    pub arguments: Vec<Type>,
}

#[derive(Debug)]
pub struct EnvironmentInstanceSchema {
    pub type_variable_count: TypeVariableIdentifier,
    pub constraints: Vec<EnvironmentInstance>,
    pub arguments: Vec<Type>,
}

#[derive(Debug)]
pub struct EnvironmentClass {
    pub function_dummy_class: bool,
    pub arity: TypeVariableIdentifier,
    pub functions: HashSet<FunctionIdentifier>,
    pub instance_schemas: Vec<EnvironmentInstanceSchema>, //Indices: InstanceIdentifier
}

#[derive(Debug, Copy, Clone)]
pub struct EnvironmentFunction {
    pub class: ClassIdentifier,
    pub identifier: FunctionIdentifier,
}

pub struct GlobalEnvironment {
    pub output: TypedFile,

    pub data_identifiers: HashMap<String, DataIdentifier>,
    pub constructors: HashMap<String, (DataIdentifier, VariantIndex)>,
    pub functions: HashMap<String, EnvironmentFunction>,
    pub class_identifiers: HashMap<String, ClassIdentifier>,

    pub classes: Vec<EnvironmentClass>,

    pub unified: HashMap<TempVariableIdentifier, Type>,

    temp_variable_counter: Cell<TempVariableIdentifier>,
    variable_counter: Cell<VariableIdentifier>,
}

impl GlobalEnvironment {
    pub fn init() -> Self {
        let dummy_function = EnvironmentFunction { class: 0, identifier: 0 };

        Self {
            output: TypedFile {
                data: vec![],
                functions: vec![],
                main: usize::MAX,
            },

            data_identifiers: HashMap::new(),
            constructors: HashMap::new(),
            functions:
            //Add the builtin function identifiers so that an error is raised
            //in case of redefinition
            [
                (String::from(builtin_names::NOT_IDENTIFIER), dummy_function),
                (String::from(builtin_names::MOD_IDENTIFIER), dummy_function),
                (String::from(builtin_names::PURE_IDENTIFIER), dummy_function),
                (String::from(builtin_names::LOG_IDENTIFIER), dummy_function),
                (String::from(builtin_names::UNIT_CONST_IDENTIFIER), dummy_function),
            ].into_iter().collect(),
            class_identifiers: HashMap::new(),

            classes: Vec::new(),
            unified: HashMap::new(),

            temp_variable_counter: Cell::new(0),
            variable_counter: Cell::new(0),
        }
    }

    pub fn next_temp_variable(&self) -> TempVariableIdentifier {
        let identifier = self.temp_variable_counter.get();
        self.temp_variable_counter.set(identifier + 1);
        identifier
    }

    pub fn next_variable(&self) -> VariableIdentifier {
        let identifier = self.variable_counter.get();
        self.variable_counter.set(identifier + 1);
        identifier
    }
}

#[derive(Clone)]
pub struct LocalEnvironment<'a> {
    pub type_variables: &'a HashMap<String, TypeVariableIdentifier>,
    pub variables: MapM<String, (VariableIdentifier, Type)>, //Immutable
    pub instances: &'a [EnvironmentInstance] //Indices are InstanceVariableIdentifiers
}

impl<'a> LocalEnvironment<'a> {
    pub fn with_type_variables(
        type_variables: &'a HashMap<String, TypeVariableIdentifier>,
    ) -> Self {
        Self {
            type_variables,
            variables: MapM::new(),
            instances: &[],
        }
    }

    pub fn add_variable(&self, name: String, identifier: VariableIdentifier, r#type: Type) -> Self {
        let (variables, _) = self.variables.insert(name, (identifier, r#type));
        Self {
            type_variables: self.type_variables,
            variables,
            instances: self.instances,
        }
    }

    pub fn set_instances(&self, instances: &'a [EnvironmentInstance]) -> Self {
        Self {
            type_variables: self.type_variables,
            variables: self.variables.clone(),
            instances,
        }
    }
}
