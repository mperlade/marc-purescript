use std::fmt;
use std::fmt::{Display, Formatter};
use num_bigint::BigInt;
use crate::error;
use crate::parser::ast::AstLocation;
use crate::typing::typed_file::Type;

#[derive(Debug)]
pub enum TypeError {
    UnrecognizedIdentifier(AstLocation, String),
    IllFormedType {
        location: AstLocation,
        identifier: String,
        found_arity: usize,
        expected_arity: usize,
    },
    InvalidType {
        location: AstLocation,
        found_type: Type,
        expected_types: Vec<Type>,
    },
    IntegerNotInRange(AstLocation, BigInt),
    IdentifierIsNotAFunction(AstLocation, String),
    ApplicationBadArity {
        location: AstLocation,
        identifier: String,
        found_arity: usize,
        expected_arity: usize,
    },
    InstanceUndeterminedTypes(AstLocation),
    ResolutionFailed(AstLocation),
    ConstructorBadArity {
        location: AstLocation,
        identifier: String,
        found_arity: usize,
        expected_arity: usize,
    },
    PatternRepeatingVariable(AstLocation, String),
    NonExhaustivePatternMatching(AstLocation),
    FunctionDefinitionWithoutTypeDeclaration(AstLocation, String),
    FunctionTypeDeclarationWithoutDefinition(AstLocation, String),
    FunctionDeclaredTwice(AstLocation, String),
    FunctionBadNumberOfArguments {
        location: AstLocation,
        identifier: String,
        found_arguments: usize,
        expected_arguments: usize,
    },
    AtLeastTwoArgumentsAreNotVariables(AstLocation, String),
    NonVariableArgumentIndexDiffers(AstLocation, String),
    GlobalVariableMultipleDefinitions(AstLocation, String),
    NonUnifiedTypesInFunction(AstLocation),
    DataDeclarationTypeVariablesSameName(AstLocation, String),
    TypeDeclaredTwice(AstLocation, String),
    ClassDeclaredTwice(AstLocation, String),
    ConstructorDeclaredTwice(AstLocation, String),
    ClassDeclarationTypeVariablesSameName(AstLocation, String),
    ClassFunctionDeclarationInstances(AstLocation, String),
    ClassFunctionDeclarationTypeVariables(AstLocation, String),
    ClassBadArity {
        location: AstLocation,
        identifier: String,
        found_arity: usize,
        expected_arity: usize,
    },
    InstanceSchemasCouldOverlap(AstLocation),
    InstanceFunctionNotDeclaredInClass(AstLocation, String),
    InstanceFunctionMultipleDefinitions(AstLocation, String),
    InstanceIncomplete(AstLocation),
    NoMain,
    MainIsClassFunction,
    MainHasArguments(AstLocation),
    MainRequiresInstances(AstLocation),
}

impl TypeError {
    pub fn format(&self, f: &mut Formatter<'_>, file_name: &str) -> fmt::Result {
        match self {
            TypeError::UnrecognizedIdentifier(location, identifier) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Unrecognized identifier \"{identifier}\". ")
            }
            TypeError::IllFormedType {
                location, identifier, found_arity, expected_arity
            } => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Ill formed type. Type \"{identifier}\" has arity {expected_arity}, \
                    found {found_arity} type arguments. ")
            }
            TypeError::InvalidType {
                location, found_type, expected_types
            } => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Invalid type. Found {found_type}, expected ")?;
                if expected_types.len() == 1 {
                    write!(f, "{}. ", expected_types[0])
                } else {
                    for expected_type in &expected_types[0..expected_types.len() - 2] {
                        write!(f, "{}, ", expected_type)?;
                    }
                    write!(f, "{} or {}. ",
                        expected_types[expected_types.len() - 2],
                        expected_types[expected_types.len() - 1])
                }
            }
            TypeError::IntegerNotInRange(location, integer) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Integer {integer} is not in range. Valid integers range from -2147483648 to 2147483647. ")
            }
            TypeError::IdentifierIsNotAFunction(location, identifier) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "\"{identifier}\" is not a function. ")
            }
            TypeError::ApplicationBadArity {
                location, identifier, found_arity, expected_arity
            } => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Function \"{identifier}\" expects {expected_arity} arguments, \
                    found {found_arity}. ")
            }
            TypeError::InstanceUndeterminedTypes(location) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Could not resolve instance because of undetermined types. \
                    Consider using type annotations. ")
            }
            TypeError::ResolutionFailed(location) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Could not resolve instance. ")
            }
            TypeError::ConstructorBadArity {
                location, identifier, found_arity, expected_arity
            } => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Constructor \"{identifier}\" expects {expected_arity} arguments, \
                    found {found_arity}. ")
            }
            TypeError::PatternRepeatingVariable(location, identifier) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Variable \"{identifier}\" appears more than once in the same pattern. ")
            }
            TypeError::NonExhaustivePatternMatching(location) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Pattern matching is not exhaustive. ")
            }
            TypeError::FunctionDefinitionWithoutTypeDeclaration(location, identifier) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Function \"{identifier}\" missing a signature declaration. ")
            }
            TypeError::FunctionTypeDeclarationWithoutDefinition(location, identifier) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Function \"{identifier}\" missing a definition. ")
            }
            TypeError::FunctionDeclaredTwice(location, identifier) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Function \"{identifier}\" has already been declared. ")
            }
            TypeError::FunctionBadNumberOfArguments {
                location, identifier, found_arguments, expected_arguments
            } => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Wrong number of argument in definition of function \"{identifier}\". \
                    Signature requires {expected_arguments}, found {found_arguments}. ")
            }
            TypeError::AtLeastTwoArgumentsAreNotVariables(location, identifier) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "At least two arguments in definition of function \"{identifier}\" \
                    are not variables. ")
            }
            TypeError::NonVariableArgumentIndexDiffers(location, identifier) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "The index of the the pattern-matching argument is not consistent across \
                    the definitions of function \"{identifier}\". ")
            }
            TypeError::GlobalVariableMultipleDefinitions(location, identifier) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Value {identifier} has already been defined. ")
            }
            TypeError::NonUnifiedTypesInFunction(location) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Types remain undetermined. Consider using type annotations. ")
            }
            TypeError::DataDeclarationTypeVariablesSameName(location, identifier) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Type variable \"{identifier}\" is used multiple times in type declaration. ")
            }
            TypeError::TypeDeclaredTwice(location, identifier) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Type \"{identifier}\" has already been declared. ")
            }
            TypeError::ClassDeclaredTwice(location, identifier) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Class \"{identifier}\" has already been declared. ")
            }
            TypeError::ConstructorDeclaredTwice(location, identifier) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Constructor \"{identifier}\" has already been declared. ")
            }
            TypeError::ClassDeclarationTypeVariablesSameName(location, identifier) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Type variable \"{identifier}\" is used multiple times in class declaration. ")
            }
            TypeError::ClassFunctionDeclarationInstances(location, identifier) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Class function \"{identifier}\" has instance constraints. ")
            }
            TypeError::ClassFunctionDeclarationTypeVariables(location, identifier) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Class function \"{identifier}\" has type variables. ")
            }
            TypeError::ClassBadArity {
                location, identifier, found_arity, expected_arity
            } => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Ill formed instance. Class \"{identifier}\" has arity {expected_arity}, \
                    found {found_arity} type arguments. ")
            }
            TypeError::InstanceSchemasCouldOverlap(location) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Overlapping type class instances. ")
            }
            TypeError::InstanceFunctionNotDeclaredInClass(location, identifier) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Function \"{identifier}\" is not a member of the type class. ")
            }
            TypeError::InstanceFunctionMultipleDefinitions(location, identifier) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "Definitions of function \"{identifier}\" are not contiguous. ")
            }
            TypeError::InstanceIncomplete(location) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "The instance is missing one or multiple function definitions. ")
            }
            TypeError::NoMain => {
                write!(f, "File {file_name}: \nMissing main. ")
            }
            TypeError::MainIsClassFunction => {
                write!(f, "File {file_name}: \n\"main\" is a class function. ")
            }
            TypeError::MainHasArguments(location) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "main has arguments. ")
            }
            TypeError::MainRequiresInstances(location) => {
                error::format_location(f, file_name, location.start, location.end)?;
                write!(f, "main has instance constraints. ")
            }
        }
    }
}

impl Type {
    fn needs_parentheses(&self) -> bool {
        match self {
            Type::Effect(_) => true,
            Type::DataType(_, _, children) => children.len() > 0,
            _ => false,
        }
    }
}

impl Display for Type {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Type::TempVariable(identifier) => {
                write!(f, "_t{identifier}")
            }
            Type::TypeVariable(_, name) => {
                write!(f, "{name}")
            }
            Type::Unit => {
                write!(f, "Unit")
            }
            Type::Boolean => {
                write!(f, "Boolean")
            }
            Type::Int => {
                write!(f, "Int")
            }
            Type::String => {
                write!(f, "String")
            }
            Type::Effect(r#type) => {
                if r#type.needs_parentheses() {
                    write!(f, "Effect ({type})")
                } else {
                    write!(f, "Effect {type}")
                }
            }
            Type::DataType(_, name, children) => {
                write!(f, "{name} ")?;
                for (index, child) in children.iter().enumerate() {
                    if child.needs_parentheses() {
                        write!(f, "({child})")?;
                    } else {
                        write!(f, "{child}")?;
                    }
                    if index != children.len() - 1 {
                        write!(f, " ")?;
                    }
                }
                Ok(())
            }
        }
    }
}
