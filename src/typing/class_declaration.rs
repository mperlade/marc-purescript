use crate::parser::ast::{ClassDeclaration, TypeDeclaration};
use crate::typing;
use crate::typing::environment::{ClassIdentifier, EnvironmentClass, EnvironmentFunction, GlobalEnvironment, LocalEnvironment};
use crate::typing::type_error::TypeError;
use std::collections::{HashMap, HashSet};
use crate::typing::typed_file::{FunctionIdentifier, Type, TypedFunction};

fn type_class_function(
    global_environment: &mut GlobalEnvironment,
    local_environment: &LocalEnvironment,
    type_declaration: TypeDeclaration,
    class_identifier: ClassIdentifier,
) -> Result<FunctionIdentifier, TypeError> {
    if type_declaration.constraints.len() > 0 {
        return Err(TypeError::ClassFunctionDeclarationInstances(
            type_declaration.location,
            type_declaration.identifier,
        ));
    }

    if type_declaration.type_variables.len() > 0 {
        return Err(TypeError::ClassFunctionDeclarationTypeVariables(
            type_declaration.location,
            type_declaration.identifier,
        ));
    }

    let types = type_declaration
        .types
        .into_iter()
        .map(|type_expression| {
            typing::well_formed_type(global_environment, local_environment, type_expression)
        })
        .collect::<Result<Vec<Type>, TypeError>>()?;

    let function_identifier = global_environment.output.functions.len();
    global_environment.output.functions.push(TypedFunction {
        types,
        bodies: vec![],
    });

    let environment_function = EnvironmentFunction {
        class: class_identifier,
        identifier: function_identifier,
    };

    if global_environment.functions.insert(type_declaration.identifier.clone(), environment_function).is_some() {
        return Err(TypeError::FunctionDeclaredTwice(
            type_declaration.location,
            type_declaration.identifier,
        ));
    }

    Ok(function_identifier)
}

pub fn type_class_declaration(
    global_environment: &mut GlobalEnvironment,
    class_declaration: ClassDeclaration,
) -> Result<(), TypeError> {
    //Check type variables
    let mut type_variables = HashMap::new();
    for (type_variable_identifier, name) in class_declaration.arguments.into_iter().enumerate() {
        if type_variables
            .insert(name.clone(), type_variable_identifier)
            .is_some()
        {
            return Err(TypeError::ClassDeclarationTypeVariablesSameName(
                class_declaration.location,
                name,
            ));
        }
    }
    let local_environment = LocalEnvironment::with_type_variables(&type_variables);

    //Create the class
    let class_identifier = global_environment.classes.len();

    let functions = class_declaration
        .type_declarations
        .into_iter()
        .map(|type_declaration| {
            type_class_function(global_environment, &local_environment, type_declaration, class_identifier)
        })
        .collect::<Result<HashSet<FunctionIdentifier>, TypeError>>()?;

    global_environment.classes.push(EnvironmentClass {
        function_dummy_class: false,
        arity: type_variables.len(),
        functions,
        instance_schemas: vec![],
    });

    //Add the class to the file
    if global_environment.class_identifiers.insert(class_declaration.name.clone(), class_identifier).is_some() {
       return Err(TypeError::ClassDeclaredTwice(
           class_declaration.location,
           class_declaration.name
       ));
    }

    Ok(())
}
