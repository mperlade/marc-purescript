use std::collections::HashMap;
use crate::parser::ast::{AstLocation, Definition};
use crate::typing::environment::{GlobalEnvironment, LocalEnvironment};
use crate::typing::{expression, pattern, pattern_matching, unification};
use crate::typing::type_error::TypeError;
use crate::typing::typed_file::{FunctionBody, Type, TypedExpression, TypedPattern, VariableIdentifier};

pub fn type_function_body(
    global_environment: &mut GlobalEnvironment,
    local_environment: &LocalEnvironment,
    location: AstLocation,
    expected_types: &[Type],
    definitions: Vec<Definition>,
) -> Result<FunctionBody, TypeError> {
    assert_ne!(definitions.len(), 0);

    let mut non_variable_argument_index = None;
    let mut should_not_redefine = false;

    let mut branches = vec![];
    for definition in definitions {
        //Check that the number of arguments is correct
        if definition.arguments.len() != expected_types.len() - 1 {
            return Err(TypeError::FunctionBadNumberOfArguments {
                location: definition.location,
                identifier: definition.identifier,
                found_arguments: definition.arguments.len(),
                expected_arguments: expected_types.len() - 1,
            });
        }

        //Check that we are not redefining a global variable
        if should_not_redefine {
            return Err(TypeError::GlobalVariableMultipleDefinitions(
                definition.location,
                definition.identifier,
            ));
        } else if expected_types.len() == 1 {
            should_not_redefine = true;
        }

        //Check that patterns are well typed, and that variables
        //do not repeat
        let mut introduced_variables = HashMap::new();
        let patterns = definition
            .arguments
            .into_iter()
            .zip(expected_types.iter())
            .map(|(pattern, expected_type)| {
                let pattern_location = pattern.get_location();
                let typed_pattern = pattern::type_pattern_recursive(
                    global_environment,
                    &mut introduced_variables,
                    pattern,
                )?;
                unification::assert_unification(
                    pattern_location,
                    &mut global_environment.unified,
                    &typed_pattern.get_type(),
                    expected_type,
                )?;
                Ok(typed_pattern)
            })
            .collect::<Result<Vec<TypedPattern>, TypeError>>()?;

        let mut local_environment = local_environment.clone();
        for (name, (identifier, r#type)) in introduced_variables {
            local_environment =
                local_environment.add_variable(name, identifier, r#type);
        }

        //Check that at most one argument is not a variable,
        //and that the index of this argument (if it exists) does not differ between definitions
        let not_variable_indices =
            patterns.iter().enumerate().filter_map(|(index, pattern)| {
                if let TypedPattern::Variable(_, _) = pattern {
                    None
                } else {
                    Some(index)
                }
            });
        let mut already_one = false;
        for index in not_variable_indices {
            if already_one {
                return Err(TypeError::AtLeastTwoArgumentsAreNotVariables(
                    definition.location,
                    definition.identifier,
                ));
            }
            already_one = true;
            if let Some(current_index) = non_variable_argument_index {
                if current_index != index {
                    return Err(TypeError::NonVariableArgumentIndexDiffers(
                        definition.location,
                        definition.identifier,
                    ));
                }
            } else {
                non_variable_argument_index = Some(index);
            }
        }

        //Check that the expression is well typed
        let expression_location = definition.expression.get_location();
        let typed_expression = expression::type_expression(
            global_environment,
            &local_environment,
            definition.expression,
        )?;
        unification::assert_unification(
            expression_location,
            &mut global_environment.unified,
            &typed_expression.get_type(),
            expected_types.last().unwrap(),
        )?;

        branches.push((patterns, typed_expression));
    }

    let mut body = if let Some(index) = non_variable_argument_index {
        //One argument is not a variable, we add an artificial case ... of
        let fake_arguments: Vec<VariableIdentifier> = (0..expected_types.len() - 1)
            .map(|_| global_environment.next_variable())
            .collect();

        let case_of_branches: Vec<(TypedPattern, TypedExpression)> = branches
            .into_iter()
            .map(|(patterns, expression)| {
                let mut real_pattern = None;
                let assignments = patterns
                    .into_iter()
                    .enumerate()
                    .filter_map(|(argument_index, pattern)| {
                        if argument_index == index {
                            real_pattern = Some(pattern);
                            None
                        } else {
                            if let TypedPattern::Variable(_, identifier) = pattern {
                                Some((
                                    identifier,
                                    TypedExpression::LocalVariable(
                                        expected_types[argument_index].clone(),
                                        fake_arguments[argument_index],
                                    ),
                                ))
                            } else {
                                unreachable!();
                            }
                        }
                    })
                    .collect();

                (
                    real_pattern.unwrap(),
                    TypedExpression::Let(
                        expected_types.last().unwrap().clone(),
                        assignments,
                        Box::new(expression),
                    ),
                )
            })
            .collect();

        //Check exhaustiveness
        let pattern_iterator = case_of_branches.iter().map(|(pattern, _)| pattern);
        if !pattern_matching::check_exhaustiveness(&global_environment, pattern_iterator) {
            return Err(TypeError::NonExhaustivePatternMatching(
                location,
            ));
        }

        let pattern_matching_input = fake_arguments[index];
        FunctionBody {
            instance_variable_count: local_environment.instances.len(),
            arguments: fake_arguments,
            expression: TypedExpression::Case(
                expected_types.last().unwrap().clone(),
                Box::new(TypedExpression::LocalVariable(
                    expected_types[index].clone(),
                    pattern_matching_input,
                )),
                case_of_branches,
            ),
        }
    } else {
        //All arguments are always variables, only the first definition matters
        //(this is the branch taken for "normal" functions, ie. with only one definition
        let (arguments, expression) = branches.into_iter().next().unwrap();

        //We know that all arguments are only variables, so we just have to extract
        let argument_variables = arguments
            .into_iter()
            .map(|pattern| {
                if let TypedPattern::Variable(_, identifier) = pattern {
                    identifier
                } else {
                    unreachable!();
                }
            })
            .collect();

        FunctionBody {
            instance_variable_count: local_environment.instances.len(),
            arguments: argument_variables,
            expression,
        }
    };

    if !unification::expression_apply_unified(&global_environment.unified, &mut body.expression) {
        return Err(TypeError::NonUnifiedTypesInFunction(location));
    }

    Ok(body)
}