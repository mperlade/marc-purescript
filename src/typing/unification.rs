use crate::typing::environment::{GlobalEnvironment, TempVariableIdentifier};
use crate::typing::typed_file::{Type, TypedExpression, TypedPattern, TypeVariableIdentifier};
use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::rc::Rc;
use crate::parser::ast::AstLocation;
use crate::typing::type_error::TypeError;

pub trait Unified {
    fn insert_or_get(&mut self, temp_variable: TempVariableIdentifier, r#type: &Type) -> Option<Type>;
}

impl Unified for HashMap<TempVariableIdentifier, Type> {
    fn insert_or_get(&mut self, temp_variable: TempVariableIdentifier, r#type: &Type) -> Option<Type> {
        match self.entry(temp_variable) {
            Entry::Occupied(e) => {
                Some(e.get().clone())
            }
            Entry::Vacant(e) => {
                e.insert(r#type.clone());
                None
            }
        }
    }
}

pub struct NonDestructiveUnified<'a> {
    unified: &'a mut HashMap<TempVariableIdentifier, Type>,
    newly_unified: HashMap<TempVariableIdentifier, Type>,
}

impl<'a> Unified for NonDestructiveUnified<'a> {
    fn insert_or_get(&mut self, temp_variable: TempVariableIdentifier, r#type: &Type) -> Option<Type> {
        if let Some(unified_type) = self.unified.get(&temp_variable) {
            Some(unified_type.clone())
        } else {
            self.newly_unified.insert_or_get(temp_variable, r#type)
        }
    }
}

impl<'a> NonDestructiveUnified<'a> {
    pub fn new(unified: &'a mut HashMap<TempVariableIdentifier, Type>) -> Self {
        Self {
            unified,
            newly_unified: HashMap::new(),
        }
    }

    pub fn apply(self) {
        self.unified.extend(self.newly_unified.into_iter());
    }
}

pub fn unify_types_recursive<U: Unified>(
    unified: &mut U,
    a: &Type,
    b: &Type,
) -> bool {
    match (a, b) {
        (Type::Unit, Type::Unit) => true,
        (Type::Boolean, Type::Boolean) => true,
        (Type::Int, Type::Int) => true,
        (Type::String, Type::String) => true,
        (Type::Effect(a), Type::Effect(b)) => unify_types_recursive(unified, a, b),
        (Type::TypeVariable(a_identifier, _), Type::TypeVariable(b_identifier, _)) => {
            *a_identifier == *b_identifier
        }
        (Type::DataType(a_data, _, a_types), Type::DataType(b_data, _, b_types)) => {
            *a_data == *b_data
                && a_types
                    .iter()
                    .zip(b_types.iter())
                    .all(|(a, b)| unify_types_recursive(unified, a, b))
        }
        (Type::TempVariable(a_identifier), _) => if let Some(unified_type) = unified.insert_or_get(*a_identifier, b) {
            unify_types_recursive(unified, &unified_type, b)
        } else {
            true
        },
        (_, Type::TempVariable(_)) => unify_types_recursive(unified, b, a),
        _ => false,
    }
}

pub fn assert_unification(
    location: AstLocation,
    unified: &mut HashMap<TempVariableIdentifier, Type>,
    found: &Type,
    expected: &Type,
) -> Result<(), TypeError> {
    if unify_types_recursive(unified, found, expected) {
        Ok(())
    } else {
        Err(TypeError::InvalidType {
            location,
            found_type: apply_unified(unified, found),
            expected_types: vec![apply_unified(unified, expected)],
        })
    }
}

pub fn is_fully_unified(unified: &HashMap<TempVariableIdentifier, Type>, r#type: &Type) -> bool {
    match r#type {
        Type::TempVariable(identifier) => {
            if let Some(new_type) = unified.get(&identifier) {
                is_fully_unified(unified, &new_type)
            } else {
                false
            }
        }
        Type::DataType(_, _, types) => {
            for r#type in types.iter() {
                if !is_fully_unified(unified, r#type) {
                    return false;
                }
            }
            true
        }
        Type::Effect(r#type) => {
            is_fully_unified(unified, r#type)
        }
        _ => true,
    }
}

pub fn apply_unified(unified: &HashMap<TempVariableIdentifier, Type>, r#type: &Type) -> Type {
    match r#type {
        Type::TempVariable(identifier) => {
            if let Some(new_type) = unified.get(identifier) {
                apply_unified(unified, new_type)
            } else {
                Type::TempVariable(*identifier)
            }
        }
        Type::Effect(r#type) => {
            Type::Effect(Rc::new(apply_unified(unified, r#type)))
        }
        Type::DataType(identifier, name, types) => {
            Type::DataType(*identifier, name.clone(), Rc::from(
                types.iter()
                    .map(|r#type| apply_unified(unified, r#type))
                    .collect::<Vec<Type>>()
            ))
        }
        _ => r#type.clone(),
    }
}

fn apply_unified_strict(
    unified: &HashMap<TempVariableIdentifier, Type>,
    r#type: &Type,
) -> Option<Type> {
    Some(match r#type {
        Type::TempVariable(identifier) => {
            if let Some(new_type) = unified.get(identifier) {
                apply_unified_strict(unified, new_type)?
            } else {
                return None;
            }
        }
        Type::Effect(r#type) => {
            Type::Effect(Rc::new(apply_unified_strict(unified, r#type)?))
        }
        Type::DataType(identifier, name, types) => {
            Type::DataType(*identifier, name.clone(), Rc::from(
                types.iter()
                    .map(|r#type| apply_unified_strict(unified, r#type))
                    .collect::<Option<Vec<Type>>>()?
            ))
        }
        _ => r#type.clone(),
    })
}

fn pattern_apply_unified(
    unified: &HashMap<TempVariableIdentifier, Type>,
    pattern: &mut TypedPattern
) -> bool {
    let type_mut = pattern.get_type_mut();
    *type_mut = if let Some(r#type) = apply_unified_strict(unified, type_mut) {
        r#type
    } else {
        return false;
    };
    match pattern {
        TypedPattern::Constructor(_, _, _, patterns) => {
            patterns
                .iter_mut()
                .all(|pattern| {
                    pattern_apply_unified(unified, pattern)
                })
        }
        TypedPattern::Constant(_, _)
        | TypedPattern::Variable(_, _) => true
    }
}

pub fn expression_apply_unified(
    unified: &HashMap<TempVariableIdentifier, Type>,
    expression: &mut TypedExpression,
) -> bool {
    let type_mut = expression.get_type_mut();
    *type_mut = if let Some(r#type) = apply_unified_strict(unified, type_mut) {
        r#type
    } else {
        return false;
    };
    match expression {
        TypedExpression::Constructor(_, _, _, expressions)
        | TypedExpression::Application(_, _, _, expressions)
        | TypedExpression::Do(_, expressions) => {
            expressions
                .iter_mut()
                .all(|expression| {
                    expression_apply_unified(unified, expression)
                })
        }
        TypedExpression::UnaryMinus(_, expression)
        | TypedExpression::Not(_, expression)
        | TypedExpression::Log(_, expression)
        | TypedExpression::Pure(_, expression)
        | TypedExpression::IntToString(_, expression) => {
            expression_apply_unified(unified, &mut *expression)
        }
        TypedExpression::BinaryOperation(_, a, _, b)
        | TypedExpression::Mod(_, a, b) => {
            expression_apply_unified(unified, &mut *a)
                && expression_apply_unified(unified, &mut *b)
        }
        TypedExpression::IfThenElse(_, a, b, c) => {
            expression_apply_unified(unified, &mut *a)
                && expression_apply_unified(unified, &mut *b)
                && expression_apply_unified(unified, &mut *c)
        }
        TypedExpression::Let(_, assignments, expression) => {
            assignments
                .iter_mut()
                .all(|(_, expression)| {
                    expression_apply_unified(unified, expression)
                })
                && expression_apply_unified(unified, &mut *expression)
        }
        TypedExpression::Case(_, expression, branches) => {
            expression_apply_unified(unified, expression)
                && branches
                .iter_mut()
                .all(|(pattern, expression)| {
                    pattern_apply_unified(unified, pattern)
                        && expression_apply_unified(unified, expression)
                })
        }
        TypedExpression::Constant(_, _)
        | TypedExpression::LocalVariable(_, _)
        | TypedExpression::Unit(_) => true
    }
}

pub fn temp_type_variables(
    global_environment: &GlobalEnvironment,
    replacements: &mut HashMap<TypeVariableIdentifier, TempVariableIdentifier>,
    r#type: &Type,
) -> Type {
    //Replaces type variables with temp variable
    match r#type {
        Type::TypeVariable(identifier, _) => {
            match replacements.entry(*identifier) {
                Entry::Occupied(e) => {
                    Type::TempVariable(*e.get())
                }
                Entry::Vacant(e) => {
                    let temp_variable_identifier = global_environment.next_temp_variable();
                    e.insert(temp_variable_identifier);
                    Type::TempVariable(temp_variable_identifier)
                }
            }
        }
        Type::Effect(r#type) => {
            Type::Effect(Rc::new(temp_type_variables(global_environment, replacements, r#type)))
        }
        Type::DataType(identifier, name, types) => {
            Type::DataType(*identifier, name.clone(), Rc::from(
                types.iter()
                    .map(|r#type| temp_type_variables(global_environment, replacements, r#type))
                    .collect::<Vec<Type>>()
            ))
        }
        _ => r#type.clone(),
    }
}
