use crate::parser::ast::{AstLocation, BinaryOperation, Expression};
use crate::typing;
use crate::typing::environment::{
    ClassIdentifier, GlobalEnvironment, LocalEnvironment,
};
use crate::typing::type_error::TypeError;
use crate::typing::typed_file::{InstanceResolution, Type, TypedExpression, TypedPattern, VariableIdentifier};
use crate::typing::unification::NonDestructiveUnified;
use crate::typing::{builtin_names, constant, pattern, pattern_matching, unification};
use std::collections::HashMap;
use std::rc::Rc;

fn resolve_instance(
    global_environment: &mut GlobalEnvironment,
    local_environment: &LocalEnvironment,
    location: AstLocation,
    class: ClassIdentifier,
    arguments: Vec<Type>,
) -> Result<Option<InstanceResolution>, TypeError> {
    //First we check that all variables are unified
    if !arguments
        .iter()
        .all(|argument| unification::is_fully_unified(&global_environment.unified, argument))
    {
        return Err(TypeError::InstanceUndeterminedTypes(location));
    }

    //Then we try to find a schema that matches the instance
    'next_schema: for (instance_identifier, schema) in global_environment.classes[class]
        .instance_schemas
        .iter()
        .enumerate()
    {
        let mut replacements = HashMap::new();
        let schema_arguments: Vec<Type> = schema
            .arguments
            .iter()
            .map(|argument| {
                unification::temp_type_variables(global_environment, &mut replacements, argument)
            })
            .collect();

        let mut unified = NonDestructiveUnified::new(&mut global_environment.unified);
        for (argument, schema_argument) in arguments.iter().zip(schema_arguments.iter()) {
            if !unification::unify_types_recursive(&mut unified, argument, schema_argument) {
                continue 'next_schema;
            }
        }

        //Schema successfully unified
        unified.apply();

        //Recursively resolve
        let constraints: Vec<(ClassIdentifier, Vec<Type>)> = schema
            .constraints
            .iter()
            .map(|constraint| {
                (
                    constraint.class,
                    constraint
                        .arguments
                        .iter()
                        .map(|argument| {
                            unification::temp_type_variables(
                                global_environment,
                                &mut replacements,
                                argument,
                            )
                        })
                        .collect(),
                )
            })
            .collect();

        let mut constraint_solutions = vec![];
        constraint_solutions.reserve_exact(constraints.len());
        for (class, arguments) in constraints.into_iter() {
            let resolution = resolve_instance(
                global_environment,
                local_environment,
                location,
                class,
                arguments,
            )?;
            if let Some(resolution) = resolution {
                constraint_solutions.push(resolution);
            } else {
                break 'next_schema;
            }
        }

        return Ok(Some(InstanceResolution::InstanceSchema(
            instance_identifier,
            constraint_solutions,
        )));
    }

    //No schema matches, we look in our local environment
    'next_variable: for (instance_variable_identifier, instance) in
        local_environment.instances.iter().enumerate()
    {
        if instance.class == class {
            for (argument, instance_argument) in arguments.iter().zip(instance.arguments.iter()) {
                if !unification::unify_types_recursive(
                    &mut global_environment.unified,
                    argument,
                    instance_argument,
                ) {
                    continue 'next_variable;
                }
            }

            return Ok(Some(InstanceResolution::InstanceVariable(
                instance_variable_identifier,
            )));
        }
    }

    //Instance cannot be resolved
    Ok(None)
}

fn resolve_dummy_instance(
    global_environment: &mut GlobalEnvironment,
    local_environment: &LocalEnvironment,
    location: AstLocation,
    class: ClassIdentifier,
    arguments: Vec<Type>,
) -> Result<InstanceResolution, TypeError> {
    //Unify with the only schema
    let instance_schema = &global_environment.classes[class].instance_schemas[0];
    let mut replacements = HashMap::new();
    let schema_arguments: Vec<Type> = instance_schema
        .arguments
        .iter()
        .map(|argument| {
            unification::temp_type_variables(global_environment, &mut replacements, argument)
        })
        .collect();

    for (argument, schema_argument) in arguments.iter().zip(schema_arguments.iter()) {
        unification::assert_unification(
            location,
            &mut global_environment.unified,
            argument,
            schema_argument,
        )?;
    }

    //Recursively resolve
    let constraints: Vec<(ClassIdentifier, Vec<Type>)> = instance_schema
        .constraints
        .iter()
        .map(|constraint| {
            (
                constraint.class,
                constraint
                    .arguments
                    .iter()
                    .map(|argument| {
                        unification::temp_type_variables(
                            global_environment,
                            &mut replacements,
                            argument,
                        )
                    })
                    .collect(),
            )
        })
        .collect();

    let mut constraint_solutions = vec![];
    constraint_solutions.reserve_exact(constraints.len());
    for (class, arguments) in constraints.into_iter() {
        constraint_solutions.push(
            resolve_instance(
                global_environment,
                local_environment,
                location,
                class,
                arguments,
            )?
            .ok_or(TypeError::ResolutionFailed(location))?,
        );
    }

    Ok(InstanceResolution::InstanceSchema(0, constraint_solutions))
}

fn type_function_application(
    global_environment: &mut GlobalEnvironment,
    local_environment: &LocalEnvironment,
    location: AstLocation,
    identifier: String,
    expressions: Vec<Expression>,
) -> Result<TypedExpression, TypeError> {
    //Try to match a builtin function
    if &identifier == builtin_names::NOT_IDENTIFIER {
        if expressions.len() != 1 {
            return Err(TypeError::ApplicationBadArity {
                location,
                identifier,
                found_arity: expressions.len(),
                expected_arity: 1,
            });
        }

        let expression = expressions.into_iter().next().unwrap();
        let location = expression.get_location();
        let typed_expression = type_expression(global_environment, local_environment, expression)?;
        unification::assert_unification(
            location,
            &mut global_environment.unified,
            typed_expression.get_type(),
            &Type::Boolean,
        )?;

        return Ok(TypedExpression::Not(
            Type::Boolean,
            Box::new(typed_expression),
        ));
    }
    if &identifier == builtin_names::MOD_IDENTIFIER {
        if expressions.len() != 2 {
            return Err(TypeError::ApplicationBadArity {
                location,
                identifier,
                found_arity: expressions.len(),
                expected_arity: 2,
            });
        }

        let mut expression_iter = expressions.into_iter();
        let a = expression_iter.next().unwrap();
        let b = expression_iter.next().unwrap();
        let location_a = a.get_location();
        let location_b = b.get_location();
        let typed_a = type_expression(global_environment, local_environment, a)?;
        let typed_b = type_expression(global_environment, local_environment, b)?;
        unification::assert_unification(
            location_a,
            &mut global_environment.unified,
            typed_a.get_type(),
            &Type::Int,
        )?;
        unification::assert_unification(
            location_b,
            &mut global_environment.unified,
            typed_b.get_type(),
            &Type::Int,
        )?;

        return Ok(TypedExpression::Mod(
            Type::Int,
            Box::new(typed_a),
            Box::new(typed_b),
        ));
    }
    if &identifier == builtin_names::PURE_IDENTIFIER {
        if expressions.len() != 1 {
            return Err(TypeError::ApplicationBadArity {
                location,
                identifier,
                found_arity: expressions.len(),
                expected_arity: 1,
            });
        }

        let expression = expressions.into_iter().next().unwrap();
        let typed_expression = type_expression(global_environment, local_environment, expression)?;

        return Ok(TypedExpression::Pure(
            Type::Effect(Rc::new(typed_expression.get_type().clone())),
            Box::new(typed_expression)
        ));
    }
    if &identifier == builtin_names::LOG_IDENTIFIER {
        if expressions.len() != 1 {
            return Err(TypeError::ApplicationBadArity {
                location,
                identifier,
                found_arity: expressions.len(),
                expected_arity: 1,
            });
        }

        let expression = expressions.into_iter().next().unwrap();
        let location = expression.get_location();
        let typed_expression = type_expression(global_environment, local_environment, expression)?;
        unification::assert_unification(
            location,
            &mut global_environment.unified,
            typed_expression.get_type(),
            &Type::String,
        )?;

        return Ok(TypedExpression::Log(
            Type::Effect(Rc::new(Type::Unit)),
            Box::new(typed_expression),
        ));
    }
    if &identifier == builtin_names::INT_TO_STRING_IDENTIFIER {
        if expressions.len() != 1 {
            return Err(TypeError::ApplicationBadArity {
                location,
                identifier,
                found_arity: expressions.len(),
                expected_arity: 1,
            });
        }

        let expression = expressions.into_iter().next().unwrap();
        let location = expression.get_location();
        let typed_expression = type_expression(global_environment, local_environment, expression)?;

        unification::assert_unification(
            location,
            &mut global_environment.unified,
            typed_expression.get_type(),
            &Type::Int,
        )?;

        return Ok(TypedExpression::IntToString(
            Type::String,
            Box::new(typed_expression),
        ));
    }
    if &identifier == builtin_names::UNIT_CONST_IDENTIFIER {
        if expressions.len() != 0 {
            return Err(TypeError::ApplicationBadArity {
                location,
                identifier,
                found_arity: expressions.len(),
                expected_arity: 0,
            });
        }

        return Ok(TypedExpression::Unit(Type::Unit));
    }

    //Check that the function exists
    let function =
        *global_environment
            .functions
            .get(&identifier)
            .ok_or(TypeError::UnrecognizedIdentifier(
                location,
                identifier.clone(),
            ))?;

    //Check that the arity is correct
    let expected_arguments = global_environment.output.functions[function.identifier]
        .types
        .len()
        - 1;
    if expressions.len() != expected_arguments {
        return Err(TypeError::ApplicationBadArity {
            location,
            identifier,
            found_arity: expressions.len(),
            expected_arity: expected_arguments,
        });
    }

    //Type the expressions
    let typed_expressions = expressions
        .into_iter()
        .map(|expression| type_expression(global_environment, local_environment, expression))
        .collect::<Result<Vec<TypedExpression>, TypeError>>()?;

    //Unify with the declared type
    let mut replacements = HashMap::new();
    let generic_types: Vec<Type> = global_environment.output.functions[function.identifier]
        .types
        .iter()
        .map(|r#type| {
            unification::temp_type_variables(global_environment, &mut replacements, r#type)
        })
        .collect();

    for (typed_expression, expected) in typed_expressions.iter().zip(generic_types.iter()) {
        let found = typed_expression.get_type();
        unification::assert_unification(
            location,
            &mut global_environment.unified,
            found,
            expected,
        )?;
    }

    //We can now get all the type constraints on the instance
    let class = &global_environment.classes[function.class];
    let arguments: Vec<Type> = (0..class.arity)
        .map(|type_variable| {
            Type::TempVariable(
                if let Some(temp_variable) = replacements.get(&type_variable) {
                    *temp_variable
                } else {
                    global_environment.next_temp_variable()
                },
            )
        })
        .collect();

    //The last thing to do is resolve the instance
    let output_type = generic_types.last().unwrap().clone();
    let instance_resolution = if class.function_dummy_class {
        resolve_dummy_instance(
            global_environment,
            local_environment,
            location,
            function.class,
            arguments,
        )?
    } else {
        resolve_instance(
            global_environment,
            local_environment,
            location,
            function.class,
            arguments,
        )?
        .ok_or(TypeError::ResolutionFailed(location))?
    };
    Ok(TypedExpression::Application(
        output_type,
        instance_resolution,
        function.identifier,
        typed_expressions,
    ))
}

pub fn type_expression(
    global_environment: &mut GlobalEnvironment,
    local_environment: &LocalEnvironment,
    expression: Expression,
) -> Result<TypedExpression, TypeError> {
    Ok(match expression {
        Expression::Constant(location, literal) => {
            let (r#type, constant) = constant::type_constant(location, literal)?;
            TypedExpression::Constant(r#type, constant)
        }
        Expression::UnaryMinus(location, expression)
        if constant::is_integer_constant(&*expression) => {
            //The only use of this is to be able to type -2147483648
            let constant = constant::type_minus_int(location, *expression)?;
            TypedExpression::Constant(Type::Int, constant)
        }
        Expression::Identifier(location, identifier) => {
            if let Some((variable_identifier, variable_type)) =
                local_environment.variables.get(&identifier)
            {
                TypedExpression::LocalVariable(variable_type.clone(), *variable_identifier)
            } else {
                type_function_application(
                    global_environment,
                    local_environment,
                    location,
                    identifier,
                    vec![],
                )?
            }
        }
        Expression::Constructor(location, identifier, expressions) => {
            if let Some((data_identifier, variant_index)) =
                global_environment.constructors.get(&identifier)
            {
                let (data_identifier, variant_index) = (*data_identifier, *variant_index);
                let data = &global_environment.output.data[data_identifier];
                let expected_arity = data.variants[variant_index].len();
                if expected_arity != expressions.len() {
                    return Err(TypeError::ConstructorBadArity {
                        location,
                        identifier,
                        found_arity: expressions.len(),
                        expected_arity,
                    });
                }
                let name = data.name.clone();
                let arity = data.arity;

                let mut replacements = HashMap::new();
                let expected_types: Vec<Type> = data.variants[variant_index]
                    .iter()
                    .map(|r#type| {
                        unification::temp_type_variables(
                            global_environment,
                            &mut replacements,
                            r#type,
                        )
                    })
                    .collect();

                let typed_expressions = expected_types
                    .iter()
                    .zip(expressions.into_iter())
                    .map(|(expected_type, argument)| {
                        let location = argument.get_location();
                        let typed =
                            type_expression(global_environment, local_environment, argument)?;
                        unification::assert_unification(
                            location,
                            &mut global_environment.unified,
                            typed.get_type(),
                            &expected_type,
                        )?;
                        Ok(typed)
                    })
                    .collect::<Result<Vec<TypedExpression>, TypeError>>()?;

                let types = (0..arity)
                    .map(|type_variable| {
                        Type::TempVariable(
                            if let Some(temp_variable) = replacements.get(&type_variable) {
                                *temp_variable
                            } else {
                                global_environment.next_temp_variable()
                            },
                        )
                    })
                    .collect::<Vec<Type>>();

                TypedExpression::Constructor(
                    Type::DataType(data_identifier, name, Rc::from(types)),
                    data_identifier,
                    variant_index,
                    typed_expressions,
                )
            } else {
                return Err(TypeError::UnrecognizedIdentifier(location, identifier));
            }
        }
        Expression::TypeAnnotation(_, expression, r#type) => {
            let r#type = typing::well_formed_type(global_environment, local_environment, *r#type)?;
            let location = expression.get_location();
            let typed_expression =
                type_expression(global_environment, local_environment, *expression)?;
            unification::assert_unification(
                location,
                &mut global_environment.unified,
                typed_expression.get_type(),
                &r#type,
            )?;
            typed_expression
        }
        Expression::UnaryMinus(_, expression) => {
            let location = expression.get_location();
            let typed_expression =
                type_expression(global_environment, local_environment, *expression)?;
            unification::assert_unification(
                location,
                &mut global_environment.unified,
                typed_expression.get_type(),
                &Type::Int,
            )?;
            TypedExpression::UnaryMinus(Type::Int, Box::new(typed_expression))
        }
        Expression::BinaryOperation(_, a, operation, b) => 'binop_arm: {
            let a_location = a.get_location();
            let b_location = b.get_location();
            let typed_a = type_expression(global_environment, local_environment, *a)?;
            let typed_b = type_expression(global_environment, local_environment, *b)?;

            let (expected_a_type, expected_b_type, output_type) = match operation {
                BinaryOperation::LogicalOr | BinaryOperation::LogicalAnd => {
                    (Type::Boolean, Type::Boolean, Type::Boolean)
                }
                BinaryOperation::GreaterThan
                | BinaryOperation::GreaterThanOrEqualTo
                | BinaryOperation::LessThan
                | BinaryOperation::LessThanOrEqualTo => (Type::Int, Type::Int, Type::Boolean),
                BinaryOperation::Plus
                | BinaryOperation::Minus
                | BinaryOperation::Times
                | BinaryOperation::Divide => (Type::Int, Type::Int, Type::Int),
                BinaryOperation::Concatenation => (Type::String, Type::String, Type::String),
                BinaryOperation::Equals | BinaryOperation::NotEquals => {
                    //First we ensure a and b are the same type
                    unification::assert_unification(
                        b_location,
                        &mut global_environment.unified,
                        typed_b.get_type(),
                        typed_a.get_type(),
                    )?;

                    //Then we ensure that a is a comparable type
                    if !unification::unify_types_recursive(
                        &mut global_environment.unified,
                        typed_a.get_type(),
                        &Type::Int,
                    ) && !unification::unify_types_recursive(
                        &mut global_environment.unified,
                        typed_a.get_type(),
                        &Type::Boolean,
                    ) && !unification::unify_types_recursive(
                        &mut global_environment.unified,
                        typed_a.get_type(),
                        &Type::String,
                    ) && !unification::unify_types_recursive(
                        &mut global_environment.unified,
                        typed_a.get_type(),
                        &Type::Unit
                    ) {
                        //We have to emit a custom error here, as multiple types could be expected
                        return Err(TypeError::InvalidType {
                            location: a_location,
                            found_type: typed_a.get_type().clone(),
                            expected_types: vec![Type::Int, Type::Boolean, Type::String, Type::Unit],
                        });
                    }

                    //We early break of this arm, as this is a special case
                    break 'binop_arm TypedExpression::BinaryOperation(
                        Type::Boolean,
                        Box::new(typed_a),
                        operation,
                        Box::new(typed_b),
                    );
                }
            };

            unification::assert_unification(
                a_location,
                &mut global_environment.unified,
                typed_a.get_type(),
                &expected_a_type,
            )?;
            unification::assert_unification(
                b_location,
                &mut global_environment.unified,
                typed_b.get_type(),
                &expected_b_type,
            )?;
            TypedExpression::BinaryOperation(
                output_type.clone(),
                Box::new(typed_a),
                operation,
                Box::new(typed_b),
            )
        }
        Expression::Application(location, identifier, expressions) => {
            //We check that the identifier is not a local variable
            //(we could avoid that, but it would not be compatible with Purescript)
            if local_environment.variables.get(&identifier).is_some() {
                return Err(TypeError::IdentifierIsNotAFunction(location, identifier));
            }

            type_function_application(
                global_environment,
                local_environment,
                location,
                identifier,
                expressions,
            )?
        }
        Expression::IfThenElse(_, if_expression, then_expression, else_expression) => {
            let if_location = if_expression.get_location();
            let else_location = else_expression.get_location();
            let typed_if = type_expression(global_environment, local_environment, *if_expression)?;
            let typed_then =
                type_expression(global_environment, local_environment, *then_expression)?;
            let typed_else =
                type_expression(global_environment, local_environment, *else_expression)?;
            unification::assert_unification(
                if_location,
                &mut global_environment.unified,
                typed_if.get_type(),
                &Type::Boolean,
            )?;
            unification::assert_unification(
                else_location,
                &mut global_environment.unified,
                typed_else.get_type(),
                typed_then.get_type(),
            )?;
            TypedExpression::IfThenElse(
                typed_then.get_type().clone(),
                Box::new(typed_if),
                Box::new(typed_then),
                Box::new(typed_else),
            )
        }
        Expression::Do(_, expressions) => {
            let effect_unit_type = Type::Effect(Rc::new(Type::Unit));
            let typed_expressions = expressions
                .into_iter()
                .map(|expression| {
                    let location = expression.get_location();
                    let typed_expression =
                        type_expression(global_environment, local_environment, expression)?;
                    unification::assert_unification(
                        location,
                        &mut global_environment.unified,
                        typed_expression.get_type(),
                        &effect_unit_type,
                    )?;
                    Ok(typed_expression)
                })
                .collect::<Result<Vec<TypedExpression>, TypeError>>()?;
            TypedExpression::Do(effect_unit_type, typed_expressions)
        }
        Expression::Let(_, variables, expression) => {
            let mut environment = local_environment.clone();
            let typed_assignments = variables
                .into_iter()
                .map(|(identifier, expression)| {
                    let typed = type_expression(global_environment, &environment, expression)?;
                    let variable = global_environment.next_variable();
                    if identifier != builtin_names::PATTERN_DO_NOT_CARE_IDENTIFIER {
                        environment = environment.add_variable(
                            identifier,
                            variable,
                            typed.get_type().clone(),
                        );
                    }
                    Ok((variable, typed))
                })
                .collect::<Result<Vec<(VariableIdentifier, TypedExpression)>, TypeError>>()?;
            let typed_expression = type_expression(global_environment, &environment, *expression)?;

            TypedExpression::Let(
                typed_expression.get_type().clone(),
                typed_assignments,
                Box::new(typed_expression),
            )
        }
        Expression::Case(location, expression, branches) => {
            let typed_expression =
                type_expression(global_environment, local_environment, *expression)?;
            let mut output_type = None;
            let typed_branches = branches
                .into_iter()
                .map(|(pattern, branch_expression)| {
                    let pattern_location = pattern.get_location();
                    let (typed_pattern, branch_environment) =
                        pattern::type_pattern(global_environment, local_environment, pattern)?;
                    let location = branch_expression.get_location();
                    let typed_branch_expression = type_expression(
                        global_environment,
                        &branch_environment,
                        branch_expression,
                    )?;

                    //Unify pattern and expression
                    unification::assert_unification(
                        pattern_location,
                        &mut global_environment.unified,
                        typed_pattern.get_type(),
                        typed_expression.get_type(),
                    )?;

                    //Unify expressions between one another
                    if let Some(output_type) = output_type.as_ref() {
                        unification::assert_unification(
                            location,
                            &mut global_environment.unified,
                            typed_branch_expression.get_type(),
                            output_type,
                        )?;
                    } else {
                        output_type = Some(typed_branch_expression.get_type().clone())
                    }
                    Ok((typed_pattern, typed_branch_expression))
                })
                .collect::<Result<Vec<(TypedPattern, TypedExpression)>, TypeError>>()?;

            //Check exhaustiveness
            let pattern_iterator = typed_branches.iter().map(|(pattern, _)| pattern);
            if !pattern_matching::check_exhaustiveness(global_environment, pattern_iterator) {
                return Err(TypeError::NonExhaustivePatternMatching(location));
            }

            TypedExpression::Case(
                output_type.unwrap(),
                Box::new(typed_expression),
                typed_branches,
            )
        }
    })
}
