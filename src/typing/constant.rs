use num_bigint::BigInt;
use crate::parser::ast::{AstLocation, Expression};
use crate::parser::token::Literal;
use crate::typing::type_error::TypeError;
use crate::typing::typed_file::{Constant, Type};

pub fn type_constant(location: AstLocation, literal: Literal) -> Result<(Type, Constant), TypeError> {
    //This function is used for both expressions and patterns
    Ok(match literal {
        Literal::Integer(integer) => match integer.try_into() {
            Ok(integer) => (Type::Int, Constant::Integer(integer)),
            Err(error) => {
                return Err(TypeError::IntegerNotInRange(
                    location,
                    error.into_original().into(),
                ))
            }
        },
        Literal::String(string) => (Type::String, Constant::String(string)),
        Literal::Boolean(boolean) => (Type::Boolean, Constant::Boolean(boolean)),
    })
}

pub fn is_integer_constant(expression: &Expression) -> bool {
    match expression {
        Expression::Constant(_, Literal::Integer(_)) => true,
        _ => false,
    }
}

pub fn type_minus_int(location: AstLocation, expression: Expression) -> Result<Constant, TypeError> {
    let value = match expression {
        Expression::Constant(_, Literal::Integer(value)) => value,
        _ => unreachable!(),
    };
    let signed_value: BigInt = value.into();
    let integer = -signed_value;
    match integer.try_into() {
        Ok(integer) => Ok(Constant::Integer(integer)),
        Err(error) => Err(TypeError::IntegerNotInRange(
            location,
            error.into_original()
        ))
    }
}
