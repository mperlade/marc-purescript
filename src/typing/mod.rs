use std::iter;
use crate::error::CompilerError;
use crate::parser::ast::{Ast, Declaration, Definition, Instance, TypeExpression};
use crate::typing::typed_file::{Type, TypedFile};
use environment::{GlobalEnvironment, LocalEnvironment};
use std::rc::Rc;
use type_error::TypeError;
use crate::typing::environment::EnvironmentInstance;

pub mod typed_file;
mod builtin_names;
mod class_declaration;
mod data_declaration;
mod environment;
mod function_declaration;
pub mod type_error;
mod instance_declaration;
mod unification;
mod definition;
mod pattern;
mod constant;
mod expression;
mod pattern_matching;
mod show_impl;

fn well_formed_type(
    global_environment: &GlobalEnvironment,
    local_environment: &LocalEnvironment,
    type_expression: TypeExpression,
) -> Result<Type, TypeError> {
    Ok(match type_expression {
        TypeExpression::TypeVariable(location, name) => {
            if let Some(type_variable) = local_environment.type_variables.get(&name) {
                Type::TypeVariable(*type_variable, Rc::from(name))
            } else {
                return Err(TypeError::UnrecognizedIdentifier(location, name));
            }
        }
        TypeExpression::DataType(location, name, arguments) => {
            let mut primitive_type = None;
            if name == builtin_names::UNIT_IDENTIFIER {
                primitive_type = Some(Type::Unit);
            } else if name == builtin_names::BOOLEAN_IDENTIFIER {
                primitive_type = Some(Type::Boolean);
            } else if name == builtin_names::INT_IDENTIFIER {
                primitive_type = Some(Type::Int);
            } else if name == builtin_names::STRING_IDENTIFIER {
                primitive_type = Some(Type::String);
            }
            if let Some(primitive_type) = primitive_type {
                if arguments.len() != 0 {
                    return Err(TypeError::IllFormedType {
                        location,
                        identifier: name,
                        found_arity: arguments.len(),
                        expected_arity: 0,
                    });
                } else {
                    primitive_type
                }
            } else if name == builtin_names::EFFECT_IDENTIFIER {
                if arguments.len() != 1 {
                    return Err(TypeError::IllFormedType {
                        location,
                        identifier: name,
                        found_arity: arguments.len(),
                        expected_arity: 1,
                    });
                } else {
                    let argument = arguments.into_iter().next().unwrap();
                    Type::Effect(Rc::new(well_formed_type(
                        global_environment,
                        local_environment,
                        argument,
                    )?))
                }
            } else if let Some(identifier) = global_environment.data_identifiers.get(&name) {
                let expected_arity = global_environment.output.data[*identifier].arity;
                if arguments.len() != expected_arity {
                    return Err(TypeError::IllFormedType {
                        location,
                        identifier: name,
                        found_arity: arguments.len(),
                        expected_arity,
                    });
                }
                let arguments = arguments
                    .into_iter()
                    .map(|argument| {
                        well_formed_type(global_environment, local_environment, argument)
                    })
                    .collect::<Result<Vec<Type>, TypeError>>()?;
                Type::DataType(*identifier, Rc::from(name), Rc::from(arguments))
            } else {
                return Err(TypeError::UnrecognizedIdentifier(location, name));
            }
        }
    })
}

fn well_formed_instance(
    global_environment: &GlobalEnvironment,
    local_environment: &LocalEnvironment,
    instance: Instance,
) -> Result<EnvironmentInstance, TypeError> {
    if let Some(class_identifier) = global_environment.class_identifiers.get(&instance.class) {
        let arity = global_environment.classes[*class_identifier].arity;

        if instance.arguments.len() != arity {
            return Err(TypeError::ClassBadArity {
                location: instance.location,
                identifier: instance.class,
                found_arity: instance.arguments.len(),
                expected_arity: arity,
            })
        }

        Ok(EnvironmentInstance {
            class: *class_identifier,
            arguments: instance.arguments
                .into_iter()
                .map(|argument| {
                    well_formed_type(global_environment, local_environment, argument)
                })
                .collect::<Result<Vec<Type>, TypeError>>()?,
        })
    } else {
        return Err(TypeError::UnrecognizedIdentifier(
            instance.location,
            instance.class,
        ));
    }
}

fn type_ast(file_ast: Ast) -> Result<TypedFile, TypeError> {
    let mut global_environment = GlobalEnvironment::init();

    class_declaration::type_class_declaration(
        &mut global_environment,
        show_impl::show_class(),
    ).unwrap();

    instance_declaration::type_instance_declaration(
        &mut global_environment,
        show_impl::show_boolean_instance(),
    ).unwrap();

    instance_declaration::type_instance_declaration(
        &mut global_environment,
        show_impl::show_int_instance(),
    ).unwrap();

    let mut iterator = file_ast.declarations.into_iter().peekable();
    let mut main_location = None;
    while let Some(declaration) = iterator.next() {
        match declaration {
            Declaration::Type(type_declaration) => {
                if type_declaration.identifier == builtin_names::MAIN_IDENTIFIER {
                    main_location = Some(type_declaration.location);
                }
                let definitions: Vec<Definition> = iter::from_fn(|| iterator.next_if(|declaration| {
                        if let Declaration::Definition(definition) = declaration {
                            definition.identifier == type_declaration.identifier
                        } else {
                            false
                        }
                    }))
                    .map(|declaration| if let Declaration::Definition(definition) = declaration {
                        definition
                    } else {
                        unreachable!();
                    })
                    .collect();
                if definitions.is_empty() {
                    return Err(TypeError::FunctionTypeDeclarationWithoutDefinition(
                        type_declaration.location,
                        type_declaration.identifier,
                    ));
                }
                function_declaration::type_function_declaration(&mut global_environment, type_declaration, definitions)?;
            }
            Declaration::Definition(definition) => {
                return Err(TypeError::FunctionDefinitionWithoutTypeDeclaration(
                    definition.location,
                    definition.identifier,
                ));
            }
            Declaration::Data(data_declaration) => {
                data_declaration::type_data_declaration(&mut global_environment, data_declaration)?;
            }
            Declaration::Class(class_declaration) => {
                class_declaration::type_class_declaration(
                    &mut global_environment,
                    class_declaration,
                )?;
            }
            Declaration::Instance(instance_declaration) => {
                instance_declaration::type_instance_declaration(
                    &mut global_environment,
                    instance_declaration,
                )?;
            }
        }
    }

    //Check that we have main and that it is correctly typed
    let main_function = global_environment.functions.get(builtin_names::MAIN_IDENTIFIER)
        .ok_or(TypeError::NoMain)?;
    global_environment.output.main = main_function.identifier;
    let main_class = &global_environment.classes[main_function.class];
    if !main_class.function_dummy_class {
        return Err(TypeError::MainIsClassFunction);
    }
    if !main_class.instance_schemas[0].constraints.is_empty() {
        return Err(TypeError::MainRequiresInstances(main_location.unwrap()));
    }
    let main_types = &global_environment.output.functions[main_function.identifier].types;
    if main_types.len() > 1 {
        return Err(TypeError::MainHasArguments(main_location.unwrap()));
    }
    unification::assert_unification(
        main_location.unwrap(),
        &mut global_environment.unified,
        &main_types[0],
        &Type::Effect(Rc::new(Type::Unit))
    )?;

    Ok(global_environment.output)
}

pub fn r#type(file_name: &str, ast: Ast) -> Result<TypedFile, CompilerError> {
    type_ast(ast).map_err(|error| CompilerError::TypeError {
        file_name: file_name.to_owned(),
        error
    })
}
