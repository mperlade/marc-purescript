use std::collections::HashMap;
use std::rc::Rc;
use crate::parser::ast::Pattern;
use crate::typing::{builtin_names, constant, unification};
use crate::typing::environment::{GlobalEnvironment, LocalEnvironment};
use crate::typing::type_error::TypeError;
use crate::typing::typed_file::{Type, TypedPattern, VariableIdentifier};

pub fn type_pattern_recursive(
    global_environment: &mut GlobalEnvironment,
    introduced_variables: &mut HashMap<String, (VariableIdentifier, Type)>,
    pattern: Pattern,
) -> Result<TypedPattern, TypeError> {
    Ok(match pattern {
        Pattern::Constant(location, literal) => {
            let (r#type, constant) = constant::type_constant(location, literal.clone())?;
            TypedPattern::Constant(r#type, constant)
        }
        Pattern::Identifier(location, x) => {
            let variable = global_environment.next_variable();
            let r#type = Type::TempVariable(global_environment.next_temp_variable());
            if x != builtin_names::PATTERN_DO_NOT_CARE_IDENTIFIER {
                if introduced_variables
                    .insert(x.clone(), (variable, r#type.clone()))
                    .is_some()
                {
                    return Err(TypeError::PatternRepeatingVariable(location, x.clone()));
                }
            }
            TypedPattern::Variable(r#type, variable)
        }
        Pattern::Constructor(location, identifier, patterns) => {
            if let Some((data_identifier, variant_index)) =
                global_environment.constructors.get(&identifier)
            {
                let (data_identifier, variant_index) = (*data_identifier, *variant_index);
                let data = &global_environment.output.data[data_identifier];
                let expected_arity = data.variants[variant_index].len();
                if expected_arity != patterns.len() {
                    return Err(TypeError::ConstructorBadArity {
                        location,
                        identifier,
                        found_arity: patterns.len(),
                        expected_arity,
                    });
                }
                let name = data.name.clone();
                let arity = data.arity;

                let mut replacements = HashMap::new();
                let expected_types: Vec<Type> = data.variants[variant_index]
                    .iter()
                    .map(|r#type| unification::temp_type_variables(global_environment, &mut replacements, r#type))
                    .collect();

                let typed_patterns = expected_types
                    .iter()
                    .zip(patterns.into_iter())
                    .map(|(expected_type, argument)| {
                        let location = argument.get_location();
                        let typed = type_pattern_recursive(
                            global_environment,
                            introduced_variables,
                            argument,
                        )?;
                        unification::assert_unification(
                            location,
                            &mut global_environment.unified,
                            typed.get_type(),
                            &expected_type,
                        )?;
                        Ok(typed)
                    })
                    .collect::<Result<Vec<TypedPattern>, TypeError>>()?;

                let types = (0..arity)
                    .map(|type_variable| {
                        Type::TempVariable(
                            if let Some(temp_variable) = replacements.get(&type_variable) {
                                *temp_variable
                            } else {
                                global_environment.next_temp_variable()
                            },
                        )
                    })
                    .collect::<Vec<Type>>();

                TypedPattern::Constructor(
                    Type::DataType(data_identifier, name, Rc::from(types)),
                    data_identifier,
                    variant_index,
                    typed_patterns,
                )
            } else {
                return Err(TypeError::UnrecognizedIdentifier(location, identifier));
            }
        }
    })
}

pub fn type_pattern<'a>(
    global_environment: &mut GlobalEnvironment,
    local_environment: &LocalEnvironment<'a>,
    pattern: Pattern,
) -> Result<(TypedPattern, LocalEnvironment<'a>), TypeError> {
    let mut introduced_variables = HashMap::new();
    let typed_pattern =
        type_pattern_recursive(global_environment, &mut introduced_variables, pattern)?;
    let mut local_environment = local_environment.clone();
    for (name, (identifier, r#type)) in introduced_variables {
        local_environment = local_environment.add_variable(name, identifier, r#type);
    }
    Ok((typed_pattern, local_environment))
}
