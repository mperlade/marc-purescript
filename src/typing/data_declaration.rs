use std::collections::HashMap;
use std::rc::Rc;
use crate::parser::ast::DataDeclaration;
use crate::typing;
use crate::typing::environment::{GlobalEnvironment, LocalEnvironment};
use crate::typing::type_error::TypeError;
use crate::typing::typed_file::{Type, TypedData};

pub fn type_data_declaration(
    global_environment: &mut GlobalEnvironment,
    data_declaration: DataDeclaration,
) -> Result<(), TypeError> {
    let mut type_variables = HashMap::new();
    for (type_variable_identifier, name) in
    data_declaration.arguments.into_iter().enumerate()
    {
        if type_variables
            .insert(name.clone(), type_variable_identifier)
            .is_some()
        {
            return Err(TypeError::DataDeclarationTypeVariablesSameName(
                data_declaration.location,
                name,
            ));
        }
    }
    let local_environment = LocalEnvironment::with_type_variables(&type_variables);

    let data_identifier = global_environment.output.data.len();
    if global_environment.data_identifiers.insert(data_declaration.name.clone(), data_identifier).is_some() {
        return Err(TypeError::TypeDeclaredTwice(
            data_declaration.location,
            data_declaration.name,
        ));
    }

    //We push the data with no variant in case this is a recursive declaration
    global_environment.output.data.push(TypedData {
        name: Rc::from(data_declaration.name.clone()),
        arity: type_variables.len(),
        variants: vec![],
    });

    let variants = data_declaration
        .constructors
        .into_iter()
        .enumerate()
        .map(|(variant_index, (constructor_name, type_expressions))| {
            //Add the constructor to the global environment
            let description = (data_identifier, variant_index);
            if global_environment.constructors.insert(constructor_name.clone(), description).is_some() {
                return Err(TypeError::ConstructorDeclaredTwice(
                    data_declaration.location,
                    constructor_name,
                ));
            }

            //Parse constructor types
            type_expressions
                .into_iter()
                .map(|type_expression| {
                    typing::well_formed_type(
                        &global_environment,
                        &local_environment,
                        type_expression,
                    )
                })
                .collect::<Result<Vec<Type>, TypeError>>()
        })
        .collect::<Result<Vec<Vec<Type>>, TypeError>>()?;

    global_environment.output.data[data_identifier].variants = variants;

    Ok(())
}
