use ndarray::{Array2, ArrayView2, Axis};
use crate::typing::environment::GlobalEnvironment;
use crate::typing::typed_file::{Constant, TypedPattern};

fn recursive_matrix_test(
    global_environment: &GlobalEnvironment,
    matrix: ArrayView2<Option<&TypedPattern>>,
) -> bool {
    let shape = matrix.shape();
    assert_ne!(shape[0], 0);
    assert_ne!(shape[1], 0);

    let mut variable_count = 0;
    let mut not_variable = None;
    for &pattern in matrix
        .slice(ndarray::s![.., 0])
        .iter()
    {
        match pattern {
            Some(TypedPattern::Variable(_, _)) | None => {
                variable_count += 1;
            }
            Some(pattern) => if not_variable.is_none() {
                not_variable = Some(pattern)
            },
        }
    }

    if let Some(not_variable) = not_variable {
        match not_variable {
            TypedPattern::Constant(_, constant) => {
                if variable_count == 0 {
                    //No variable! It's impossible to be exhaustive with constants
                    //except with boolean constants
                    if let Constant::Boolean(_) = constant {
                        for boolean in [false, true] {
                            let mut flattened_new_matrix: Vec<Option<&TypedPattern>> = Vec::new();
                            let mut row_count = 0;

                            for row in matrix.axis_iter(Axis(0)) {
                                match &row[0] {
                                    Some(TypedPattern::Variable(_, _)) | None => {
                                        flattened_new_matrix.extend(row.slice(ndarray::s![1..]).iter());
                                        row_count += 1;
                                    }
                                    Some(TypedPattern::Constant(_, Constant::Boolean(value))) => {
                                        if *value == boolean {
                                            flattened_new_matrix.extend(row.slice(ndarray::s![1..]).iter());
                                            row_count += 1;
                                        }
                                    }
                                    _ => unreachable!(),
                                }
                            }

                            if row_count == 0 {
                                return false;
                            }

                            let new_width = shape[1] - 1;
                            if new_width != 0 {
                                let new_matrix =
                                    Array2::from_shape_vec((row_count, new_width), flattened_new_matrix)
                                        .unwrap();
                                if !recursive_matrix_test(global_environment, new_matrix.view()) {
                                    return false;
                                }
                            }
                        }

                        true
                    } else {
                        false
                    }
                } else {
                    //We only have to check the rows with variables on the first column
                    if shape[1] == 1 {
                        //Only one column, with a variable
                        return true;
                    }

                    let mut flattened_new_matrix: Vec<Option<&TypedPattern>> = Vec::new();
                    flattened_new_matrix.reserve_exact(variable_count * (shape[1] - 1));
                    for row in matrix.axis_iter(Axis(0)) {
                        if let Some(TypedPattern::Variable(_, _)) | None = row[0] {
                            flattened_new_matrix.extend(row.slice(ndarray::s![1..]).iter())
                        }
                    }
                    let new_matrix =
                        Array2::from_shape_vec((variable_count, shape[1] - 1), flattened_new_matrix)
                            .unwrap();
                    recursive_matrix_test(global_environment, new_matrix.view())
                }
            }
            TypedPattern::Constructor(_, data_identifier, _, _) => {
                let variants = &global_environment.output.data[*data_identifier].variants;

                for (variant, fields) in variants.iter().enumerate() {
                    let mut flattened_new_matrix: Vec<Option<&TypedPattern>> = Vec::new();
                    let mut row_count = 0;
                    for row in matrix.axis_iter(Axis(0)) {
                        match &row[0] {
                            Some(TypedPattern::Variable(_, _)) | None => {
                                for _ in fields {
                                    //Introduce dummy variable (None)
                                    flattened_new_matrix.push(None);
                                }
                                flattened_new_matrix.extend(row.slice(ndarray::s![1..]).iter());
                                row_count += 1;
                            }
                            Some(TypedPattern::Constructor(_, _, pattern_variant, patterns)) => {
                                if *pattern_variant == variant {
                                    flattened_new_matrix.extend(patterns.iter().map(|pattern| Some(pattern)));
                                    flattened_new_matrix.extend(row.slice(ndarray::s![1..]).iter());
                                    row_count += 1;
                                }
                            }
                            Some(TypedPattern::Constant(_, _)) => unreachable!(),
                        }
                    }

                    if row_count == 0 {
                        return false;
                    }

                    let new_width = shape[1] - 1 + fields.len();
                    if new_width != 0 {
                        let new_matrix =
                            Array2::from_shape_vec((row_count, new_width), flattened_new_matrix)
                                .unwrap();
                        if !recursive_matrix_test(global_environment, new_matrix.view()) {
                            return false;
                        }
                    }
                }

                true
            }
            TypedPattern::Variable(_, _) => unreachable!(),
        }
    } else {
        //The first column is only variables
        if matrix.len_of(Axis(1)) == 1 {
            true
        } else {
            recursive_matrix_test(global_environment, matrix.slice(ndarray::s![.., 1..]))
        }
    }
}

pub fn check_exhaustiveness<'a>(
    global_environment: &GlobalEnvironment,
    patterns: impl Iterator<Item = &'a TypedPattern>,
) -> bool {
    let flattened_column: Vec<Option<&TypedPattern>> =
        patterns.map(|pattern| Some(pattern)).collect();
    let matrix = Array2::from_shape_vec((flattened_column.len(), 1), flattened_column).unwrap();
    recursive_matrix_test(global_environment, matrix.view())
}
