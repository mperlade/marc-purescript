/*

class Show a where
    show::a -> String

instance Show Boolean where
    show false = "false"
    show true = "true"

instance Show Int where
    show x = @int_to_string x

 */

use crate::parser::ast::{ClassDeclaration, Definition, Expression, Instance, InstanceDeclaration, Pattern, TypeDeclaration, TypeExpression};
use crate::parser::token::Literal;
use crate::typing::builtin_names;

pub fn show_class() -> ClassDeclaration {
    ClassDeclaration {
        location: Default::default(),
        name: String::from(builtin_names::SHOW_CLASS_IDENTIFIER),
        arguments: vec![String::from("a")],
        type_declarations: vec![TypeDeclaration {
            location: Default::default(),
            identifier: String::from(builtin_names::SHOW_IDENTIFIER),
            type_variables: vec![],
            constraints: vec![],
            types: vec![
                TypeExpression::TypeVariable(Default::default(), String::from("a")),
                TypeExpression::DataType(
                    Default::default(),
                    String::from(builtin_names::STRING_IDENTIFIER),
                    vec![],
                ),
            ],
        }],
    }
}

pub fn show_boolean_instance() -> InstanceDeclaration {
    InstanceDeclaration {
        location: Default::default (),
        constraints: vec![],
        instance: Instance {
        location: Default::default (),
        class: String::from(builtin_names::SHOW_CLASS_IDENTIFIER),
        arguments: vec![TypeExpression::DataType(
            Default::default(),
            String::from(builtin_names::BOOLEAN_IDENTIFIER),
            vec![],
        )],
        },
        definitions: vec![
            Definition {
                location: Default::default(),
                identifier: String::from(builtin_names::SHOW_IDENTIFIER),
                arguments: vec![Pattern::Constant(
                    Default::default(),
                    Literal::Boolean(false),
                )],
                expression: Expression::Constant(
                    Default::default(),
                    Literal::String(String::from("false")),
                ),
            },
            Definition {
                location: Default::default(),
                identifier: String::from(builtin_names::SHOW_IDENTIFIER),
                arguments: vec![Pattern::Constant(
                    Default::default(),
                    Literal::Boolean(true),
                )],
                expression: Expression::Constant(
                    Default::default(),
                    Literal::String(String::from("true")),
                ),
            }
        ],
    }
}

pub fn show_int_instance() -> InstanceDeclaration {
    InstanceDeclaration {
        location: Default::default(),
        constraints: vec![],
        instance: Instance {
            location: Default::default(),
            class: String::from(builtin_names::SHOW_CLASS_IDENTIFIER),
            arguments: vec![TypeExpression::DataType(
                Default::default(),
                String::from(builtin_names::INT_IDENTIFIER),
                vec![],
            )],
        },
        definitions: vec![Definition {
            location: Default::default(),
            identifier: String::from(builtin_names::SHOW_IDENTIFIER),
            arguments: vec![Pattern::Identifier(
                Default::default(),
                String::from("x"),
            )],
            expression: Expression::Application(
                Default::default(),
                String::from(builtin_names::INT_TO_STRING_IDENTIFIER),
                vec![Expression::Identifier(
                    Default::default(),
                    String::from("x"),
                )],
            ),
        }],
    }
}