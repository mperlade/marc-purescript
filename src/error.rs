use std::fmt::{Display, Formatter};
use std::{fmt, io};
use lalrpop_util::ParseError;
use lexgen_util::{LexerErrorKind, Loc};
use crate::parser::CustomParseError;
use crate::parser::token::Token;
use crate::typing::type_error::TypeError;

#[derive(Debug)]
pub enum CompilerError {
    CommandLineError,
    FileError {
        path: String,
        error: io::Error,
    },
    ParseError {
        file_name: String,
        error: ParseError<Loc, Token, CustomParseError>,
    },
    TypeError {
        file_name: String,
        error: TypeError,
    },
    SaveError(io::Error),
}

impl Display for CompilerError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            CompilerError::CommandLineError => {
                write!(f, "Error: could not parse command line arguments.")
            },
            CompilerError::FileError { path, error } => {
                write!(f, "{}: file error. {}", path, error)
            },
            CompilerError::ParseError { file_name, error } => match error {
                ParseError::InvalidToken { .. } => {
                    //Only emitted if lalrpop is used as a lexer
                    //We do not, so we can assume that this error does not exist
                    write!(f, "Invalid token! ") //Just in case
                }
                ParseError::UnrecognizedEof { location, expected } => {
                    format_location(f, file_name, *location, *location)?;
                    write!(f, "Syntax error: unexpected end of file. ")?;
                    format_expected(f, expected)
                }
                ParseError::UnrecognizedToken { token: (start, token, end), expected } => {
                    format_location(f, file_name, *start, *end)?;
                    write!(f, "Unexpected token \"{}\". ", token)?;
                    format_expected(f, expected)
                }
                ParseError::ExtraToken { token: (start, token, end) } => {
                    format_location(f, file_name, *start, *end)?;
                    write!(f, "Unexpected extra token \"{}\". ", token)
                }
                ParseError::User { error } => match error {
                    CustomParseError::LexerError(error) => {
                        format_location(f, file_name, error.location, error.location)?;
                        match &error.kind {
                            LexerErrorKind::InvalidToken => write!(f, "Invalid token"),
                            LexerErrorKind::Custom(message) => write!(f, "{}", message),
                        }
                    }
                    CustomParseError::SyntaxError(start, end, message) => {
                        format_location(f, file_name, *start, *end)?;
                        write!(f, "{}", message)
                    }
                }
            },
            CompilerError::TypeError { file_name, error,  } => {
                error.format(f, file_name)
            }
            CompilerError::SaveError(error) => {
                write!(f, "Could not save file. {}", error)
            }
        }
    }
}

pub fn format_location(f: &mut Formatter<'_>, file_name: &str, start: Loc, end: Loc) -> fmt::Result {
    write!(f, "File \"{}\", ", file_name)?;
    if start.line == end.line {
        write!(f, "line {}, ", start.line + 1)?;
        if start.col == end.col {
            writeln!(f, "character {}: ", start.col)
        } else {
            writeln!(f, "characters {}-{}: ", start.col, end.col)
        }
    } else {
        writeln!(f, "lines {}-{}:", start.line + 1, end.line + 1)
    }

}

fn better_token_name(name: &str) -> &str {
    //Very very ugly, because lalrpop outputs its "expected" tokens as strings...
    match name {
        "Case" => "\"case\"",
        "Class" => "\"class\"",
        "Data" => "\"data\"",
        "Do" => "\"do\"",
        "Else" => "\"else\"",
        "ForAll" => "\"forall\"",
        "If" => "\"if\"",
        "Import" => "\"import\"",
        "In" => "\"in\"",
        "Instance" => "\"instance\"",
        "Let" => "\"let\"",
        "Module" => "\"module\"",
        "Of" => "\"of\"",
        "Then" => "\"then\"",
        "Where" => "\"where\"",
        "LIdent" => "a lident",
        "UIdent" => "an uident",
        "Literal" => "a literal",
        "LogicalOr" => "\"||\"",
        "LogicalAnd" => "\"&&\"",
        "Equals" => "\"==\"",
        "NotEquals" => "\"/=\"",
        "GreaterThan" => "\">\"",
        "GreaterThanOrEqualTo" => "\">=\"",
        "LessThan" => "\"<\"",
        "LessThanOrEqualTo" => "\"<=\"",
        "Plus" => "\"+\"",
        "Minus" => "\"-\"",
        "Concatenation" => "\"<>\"",
        "Times" => "\"*\"",
        "Divide" => "\"/\"",
        "Assign" => "\"=\"",
        "Pipe" => "\"|\"",
        "DoubleColon" => "\"::\"",
        "Dot" => "\".\"",
        "Arrow" => "\"->\"",
        "DoubleArrow" => "\"=>\"",
        "Comma" => "\",\"",
        "LeftParenthesis" => "\"(\"",
        "RightParenthesis" => "\")\"",
        "Eof" => "\"end of file\"",
        "VirtualLeftBrace" => "\"{\"",
        "VirtualRightBrace" => "\"}\"",
        "VirtualSemicolon" => "\";\"",
        no_better_name => no_better_name,
        //Should never be reached, but I would rather not have my compiler panic
        //because I forgot one element in this monster of a list
    }
}

fn format_expected(f: &mut Formatter<'_>, expected: &Vec<String>) -> fmt::Result {
    let length = expected.len();
    if length == 0 {
        return Ok(())
    }
    if length == 1 {
        return write!(f, "Expected {}. ", better_token_name(&expected[0]))
    }
    write!(f, "Expected ")?;
    for token in &expected[0..length - 2] {
        write!(f, "{}, ", better_token_name(token))?;
    }
    write!(f, "{} or {}. ",
           better_token_name(&expected[length - 2]),
           better_token_name(&expected[length - 1]))
}