use lexgen_util::{LexerError, Loc};
use std::collections::VecDeque;
use std::str::Chars;
use crate::parser::lexer::{Lexer, Lexer_, LexerState};
use crate::parser::token::Token;

#[derive(Copy, Clone)]
enum StackElement {
    Mark, // "M"
    Block { column: i64 },
}

enum EndOfLexing {
    EndOfInput,
    LexerError(LexerError<String>),
}

pub struct WhitespaceLexer<'input> {
    lexer: Lexer_<'input, Chars<'input>, LexerState>,
    stack: Vec<StackElement>,
    output_tokens: VecDeque<(Loc, Token, Loc)>,
    last_location: Loc,
    dead: bool,
}

impl<'input> WhitespaceLexer<'input> {
    pub fn new(input: &'input str) -> Self {
        Self {
            lexer: Lexer::new_with_state(input, LexerState::init()),
            stack: Vec::new(),
            output_tokens: VecDeque::new(),
            last_location: Default::default(),
            dead: false,
        }
    }

    fn compute_column(start_location: Loc, token: &Token) -> i64 {
        if *token == Token::Eof {
            -1
        } else {
            start_location.col as i64
        }
    }

    fn read_token(&mut self) -> Result<(Loc, Token, Loc, i64), EndOfLexing> {
        if let Some(token) = self.lexer.next() {
            match token {
                Ok((start_location, token, end_location)) => {
                    let column = Self::compute_column(start_location, &token);
                    Ok((start_location, token, end_location, column))
                }
                Err(error) => Err(EndOfLexing::LexerError(error)),
            }
        } else {
            Err(EndOfLexing::EndOfInput)
        }
    }

    fn emit_token(&mut self, start_location: Loc, token: Token, end_location: Loc) {
        self.last_location = end_location;
        self.output_tokens
            .push_back((start_location, token, end_location));
    }

    fn emit_virtual_token(&mut self, token: Token) {
        self.output_tokens
            .push_back((self.last_location, token, self.last_location));
    }

    fn close(&mut self, column: i64, weak_mode: bool) {
        if weak_mode {
            return;
        }
        while let Some(&stack_top) = self.stack.last() {
            if let StackElement::Block { column: n } = stack_top {
                if n > column {
                    self.emit_virtual_token(Token::VirtualRightBrace);
                    self.stack.pop();
                } else {
                    if n == column {
                        self.emit_virtual_token(Token::VirtualSemicolon);
                    }
                    return;
                }
            } else {
                return;
            }
        }
    }

    fn unstack_down_to_mark(&mut self) {
        while let Some(element) = self.stack.pop() {
            match element {
                StackElement::Mark => return,
                StackElement::Block { .. } => self.emit_virtual_token(Token::VirtualRightBrace),
            }
        }
    }

    fn process_token(
        &mut self,
        weak_mode: bool,
        start_location: Loc,
        token: Token,
        end_location: Loc,
        column: i64,
    ) -> Result<(), EndOfLexing> {
        match token {
            Token::If | Token::LeftParenthesis | Token::Case => {
                self.close(column, weak_mode);
                self.stack.push(StackElement::Mark);
                self.emit_token(start_location, token, end_location);
            }
            Token::RightParenthesis | Token::Then | Token::Else | Token::In => {
                self.unstack_down_to_mark();
                if token == Token::Then {
                    self.stack.push(StackElement::Mark);
                }
                self.emit_token(start_location, token, end_location);
            }
            Token::Where | Token::Do | Token::Let | Token::Of => {
                self.close(column, weak_mode);
                if token == Token::Let {
                    self.stack.push(StackElement::Mark);
                }
                if token == Token::Of {
                    self.unstack_down_to_mark();
                }
                self.emit_token(start_location, token, end_location);
                self.emit_virtual_token(Token::VirtualLeftBrace);

                let (next_start_location, next_token, next_end_location, next_column) =
                    self.read_token()?;
                self.close(next_column, weak_mode);

                self.stack.push(StackElement::Block {
                    column: next_column,
                });

                self.process_token(
                    true,
                    next_start_location,
                    next_token,
                    next_end_location,
                    next_column,
                )?;
            }
            _ => {
                self.close(column, weak_mode);
                self.emit_token(start_location, token, end_location);
            }
        }

        Ok(())
    }

    fn process_next(&mut self) -> Result<(), EndOfLexing> {
        let (start_location, token, end_location, column) = self.read_token()?;
        self.process_token(false, start_location, token, end_location, column)
    }
}

impl<'input> Iterator for WhitespaceLexer<'input> {
    type Item = Result<(Loc, Token, Loc), LexerError<String>>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.dead {
            return None;
        }
        loop {
            if let Some(token) = self.output_tokens.pop_front() {
                return Some(Ok(token));
            } else {
                if let Err(end) = self.process_next() {
                    return match end {
                        EndOfLexing::EndOfInput => {
                            //On end of input, we simply terminate the iterator
                            None
                        }
                        EndOfLexing::LexerError(error) => {
                            //If there is an error, we kill the lexer and we return the error
                            self.dead = true;
                            Some(Err(error))
                        }
                    };
                }
            }
        }
    }
}
