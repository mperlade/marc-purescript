use std::mem;
use crate::parser::token;
use crate::parser::token::{Token, Literal};

pub struct LexerState {
    unescaped_string: String,
}

impl LexerState {
    pub fn init() -> Self {
        Self {
            unescaped_string: String::new(),
        }
    }
}

lexgen::lexer! {
    pub Lexer(LexerState) -> Token;

    type Error = String;

    let digit = ['0'-'9'];
    let lowercase = ['a'-'z'] | '_';
    let uppercase = ['A'-'Z'];
    let other = $lowercase | $uppercase | $digit | '\'';
    let lIdent = $lowercase $other*;
    let uIdent = $uppercase ($other | '.')*;
    let integerLiteral = '0' | ['1'-'9'] $digit*;
    let integerLiteralWithLeadingZeros = ('0' +) $integerLiteral;

    rule Init {
        [' ' '\t' '\n' '\r']+, //Do not forget \r (I'm a Windows user)

        "--" => |lexer| {
            lexer.reset_match();
            lexer.switch(LexerRule::SingleLineComment)
        },

        "{-" => |lexer| {
            lexer.reset_match();
            lexer.switch(LexerRule::MultiLineComment)
        },

        $lIdent => |lexer| {
            lexer.return_(token::match_lowercase(lexer.match_()))
        },

        $uIdent => |lexer| {
            let token = Token::UIdent(lexer.match_().to_owned());
            lexer.return_(token)
        },

        $integerLiteral => |lexer| {
            let token = Token::Literal(Literal::Integer(lexer.match_().parse().unwrap()));
            lexer.return_(token)
        },

        $integerLiteralWithLeadingZeros =? |lexer| {
            lexer.reset_match();
            lexer.return_(Err(format!("Unexpected leading zeros!")))
        },

        '\"' => |lexer| {
            lexer.reset_match();
            lexer.switch(LexerRule::StringLiteral)
        },

        "||" => |lexer| lexer.return_(Token::LogicalOr),
        "&&" => |lexer| lexer.return_(Token::LogicalAnd),
        "==" => |lexer| lexer.return_(Token::Equals),
        "/=" => |lexer| lexer.return_(Token::NotEqual),
        ">" => |lexer| lexer.return_(Token::GreaterThan),
        ">=" => |lexer| lexer.return_(Token::GreaterThanOrEqualTo),
        "<" => |lexer| lexer.return_(Token::LessThan),
        "<=" => |lexer| lexer.return_(Token::LessThanOrEqualTo),
        "+" => |lexer| lexer.return_(Token::Plus),
        "-" => |lexer| lexer.return_(Token::Minus),
        "<>" => |lexer| lexer.return_(Token::Concatenation),
        "*" => |lexer| lexer.return_(Token::Times),
        "/" => |lexer| lexer.return_(Token::Divide),
        "=" => |lexer| lexer.return_(Token::Assign),
        "|" => |lexer| lexer.return_(Token::Pipe),
        "::" => |lexer| lexer.return_(Token::DoubleColon),
        "." => |lexer| lexer.return_(Token::Dot),
        "->" => |lexer| lexer.return_(Token::Arrow),
        "=>" => |lexer| lexer.return_(Token::DoubleArrow),
        "(" => |lexer| lexer.return_(Token::LeftParenthesis),
        ")" => |lexer| lexer.return_(Token::RightParenthesis),
        "," => |lexer| lexer.return_(Token::Comma),

        $ => |lexer| lexer.return_(Token::Eof),
    }

    rule SingleLineComment {
        '\n' => |lexer| {
            lexer.reset_match();
            lexer.switch(LexerRule::Init) //Switch back to normal code
        },

        _, //We skip everything else

        $ => |lexer| lexer.return_(Token::Eof), //Do not forget to accept end-of-input
    }

    rule MultiLineComment {
        "-}" => |lexer| {
            lexer.reset_match();
            lexer.switch(LexerRule::Init) //Switch back to normal code
        },

        _, //Ignore everything else

        $ =? |lexer| {
            //We fail on end-of-input
            lexer.reset_match();
            lexer.return_(Err(format!("Unclosed multi-line comment!")))
        },
    }

    rule StringLiteral {
        '\\' (' ' | '\t' | '\n') => |lexer| {
            lexer.reset_match();
            lexer.switch(LexerRule::StringLiteralGap)
        },

        '\\' _ =? |lexer| {
            let match_ = lexer.match_();
            let character = match match_ {
                "\\\\" => Some('\\'),
                "\\\"" => Some('\"'),
                "\\n" => Some('\n'),
                _ => None,
            };
            if let Some(character) = character {
                lexer.state().unescaped_string.push(character);
                lexer.reset_match();
                lexer.continue_()
            } else {
                lexer.return_(Err(format!("Unknown escape character {}! ", match_)))
            }
        },

        '\n' | '\r' =? |lexer| {
            lexer.reset_match();
            lexer.return_(Err(format!("Line feeds are not allowed in string literals, \
                please use a gap with \\ {{line feed}} \\!")))
        },

        '\"' => |lexer| {
            lexer.reset_match();
            let token = Token::Literal(Literal::String(mem::take(&mut lexer.state().unescaped_string)));
            lexer.switch_and_return(LexerRule::Init, token)
        },

        _ => |lexer| {
            let match_ = lexer.match_();
            lexer.reset_match();
            lexer.state().unescaped_string.push_str(match_);
            lexer.continue_()
        },

        $ =? |lexer| {
            //We fail on end-of-input
            lexer.reset_match();
            lexer.return_(Err(format!("Unclosed string literal!")))
        },
    }

    rule StringLiteralGap {
        '\\' => |lexer| {
            lexer.reset_match();
            lexer.switch(LexerRule::StringLiteral)
        },

        ' ' | '\t' | '\n' | '\r', //Ignore whitespace

        _ =? |lexer| {
            let match_ = lexer.match_();
            lexer.return_(Err(format!("Unexpected character {} in gap!", match_)))
        },

        $ =? |lexer| {
            //We fail on end-of-input
            lexer.reset_match();
            lexer.return_(Err(format!("Unclosed string literal!")))
        },
    }
}
