use lexgen_util::Loc;
use crate::parser::token::Literal;

#[derive(Debug)]
pub struct Ast {
    pub declarations: Vec<Declaration>
}

#[derive(Debug, Copy, Clone, Default)]
pub struct AstLocation {
    pub start: Loc,
    pub end: Loc,
}

#[derive(Debug)]
pub enum Declaration {
    Type(TypeDeclaration),
    Definition(Definition),
    Data(DataDeclaration),
    Class(ClassDeclaration),
    Instance(InstanceDeclaration),
}

#[derive(Debug)]
pub struct TypeDeclaration {
    pub location: AstLocation,

    pub identifier: String,
    pub type_variables: Vec<String>,
    pub constraints: Vec<Instance>,
    pub types: Vec<TypeExpression>, // a -> b -> c ...
}

#[derive(Debug)]
pub struct Definition {
    pub location: AstLocation,

    pub identifier: String,
    pub arguments: Vec<Pattern>,
    pub expression: Expression,
}

#[derive(Debug)]
pub struct DataDeclaration {
    pub location: AstLocation,

    pub name: String,
    pub arguments: Vec<String>,
    pub constructors: Vec<(String, Vec<TypeExpression>)>
}

#[derive(Debug)]
pub struct ClassDeclaration {
    pub location: AstLocation,

    pub name: String,
    pub arguments: Vec<String>,
    pub type_declarations: Vec<TypeDeclaration>,
}

#[derive(Debug)]
pub struct InstanceDeclaration {
    pub location: AstLocation,

    pub constraints: Vec<Instance>,
    pub instance: Instance,
    pub definitions: Vec<Definition>,
}

#[derive(Debug)]
pub struct Instance {
    //They are called "instances" in the typing part,
    //but I decided to give them a different name in the ast, to not
    //mistake them with instance (schema) declarations
    pub location: AstLocation,

    pub class: String,
    pub arguments: Vec<TypeExpression>,
}

#[derive(Debug)]
pub enum TypeExpression {
    TypeVariable(AstLocation, String), //Lowercase identifier
    DataType(AstLocation, String, Vec<TypeExpression>), //Example: "Pair m Int" or "Boolean"
}

#[derive(Debug)]
pub enum Pattern {
    Constant(AstLocation, Literal),
    Identifier(AstLocation, String),
    Constructor(AstLocation, String, Vec<Pattern>),
}

impl Pattern {
    pub fn get_location(&self) -> AstLocation {
        match self {
            Pattern::Constant(location, _) => *location,
            Pattern::Identifier(location, _) => *location,
            Pattern::Constructor(location, _, _) => *location,
        }
    }
}

#[derive(Debug)]
pub enum Expression {
    Constant(AstLocation, Literal),
    Identifier(AstLocation, String), //Always lowercase
    Constructor(AstLocation, String, Vec<Expression>),
    TypeAnnotation(AstLocation, Box<Expression>, Box<TypeExpression>),
    UnaryMinus(AstLocation, Box<Expression>),
    BinaryOperation(AstLocation, Box<Expression>, BinaryOperation, Box<Expression>),
    Application(AstLocation, String, Vec<Expression>),
    IfThenElse(AstLocation, Box<Expression>, Box<Expression>, Box<Expression>),
    Do(AstLocation, Vec<Expression>),
    Let(AstLocation, Vec<(String, Expression)>, Box<Expression>),
    Case(AstLocation, Box<Expression>, Vec<(Pattern, Expression)>)
}

impl Expression {
    pub fn get_location(&self) -> AstLocation {
        match self {
            Expression::Constant(location, _) => *location,
            Expression::Identifier(location, _) => *location,
            Expression::Constructor(location, _, _) => *location,
            Expression::TypeAnnotation(location, _, _) => *location,
            Expression::UnaryMinus(location, _) => *location,
            Expression::BinaryOperation(location, _, _, _) => *location,
            Expression::Application(location, _, _) => *location,
            Expression::IfThenElse(location, _, _, _) => *location,
            Expression::Do(location, _) => *location,
            Expression::Let(location, _, _) => *location,
            Expression::Case(location, _, _) => *location,
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum BinaryOperation {
    LogicalOr, //Double pipe ||
    LogicalAnd, //Double ampersand &&
    Equals, // ==
    NotEquals, // /=
    GreaterThan, // >
    GreaterThanOrEqualTo, // >=
    LessThan, // <
    LessThanOrEqualTo, // <=
    Plus,
    Minus,
    Concatenation, // <>
    Times,
    Divide,
}
