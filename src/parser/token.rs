use std::fmt::{Display, Formatter};
use num_bigint::BigUint;

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Literal {
    Integer(BigUint),
    //It is not the job of the lexer to check that the integer is within bounds
    String(String),
    //Escape characters are removed
    Boolean(bool), //"true" or "false"
}

impl Display for Literal {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Literal::Integer(integer) => Display::fmt(integer, f),
            Literal::String(string) => Display::fmt(string, f),
            Literal::Boolean(boolean) => Display::fmt(boolean, f),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Token {
    Case,
    Class,
    Data,
    Do,
    Else,
    ForAll,
    If,
    Import,
    In,
    Instance,
    Let,
    Module,
    Of,
    Then,
    Where,

    LIdent(String),
    UIdent(String),

    Literal(Literal),

    LogicalOr, //Double pipe ||
    LogicalAnd, //Double ampersand &&
    Equals, // ==
    NotEqual, // /=
    GreaterThan, // >
    GreaterThanOrEqualTo, // >=
    LessThan, // <
    LessThanOrEqualTo, // <=
    Plus,
    Minus,
    Concatenation, // <>
    Times,
    Divide,

    Assign, // =
    Pipe, // |
    DoubleColon, // ::
    Dot,
    Arrow, // ->
    DoubleArrow, // =>
    Comma, //,

    LeftParenthesis,
    RightParenthesis,

    Eof,

    VirtualLeftBrace,
    VirtualRightBrace,
    VirtualSemicolon,
}

impl Display for Token {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Token::Case => write!(f, "case"),
            Token::Class => write!(f, "class"),
            Token::Data => write!(f, "data"),
            Token::Do => write!(f, "do"),
            Token::Else => write!(f, "else"),
            Token::ForAll => write!(f, "forall"),
            Token::If => write!(f, "if"),
            Token::Import => write!(f, "import"),
            Token::In => write!(f, "in"),
            Token::Instance => write!(f, "instance"),
            Token::Let => write!(f, "let"),
            Token::Module => write!(f, "module"),
            Token::Of => write!(f, "of"),
            Token::Then => write!(f, "then"),
            Token::Where => write!(f, "where"),
            Token::LIdent(identifier) => Display::fmt(identifier, f),
            Token::UIdent(identifier) => Display::fmt(identifier, f),
            Token::Literal(literal) => Display::fmt(literal, f),
            Token::LogicalOr => write!(f, "||"),
            Token::LogicalAnd => write!(f, "&&"),
            Token::Equals => write!(f, "=="),
            Token::NotEqual => write!(f, "/="),
            Token::GreaterThan => write!(f, ">"),
            Token::GreaterThanOrEqualTo => write!(f, ">="),
            Token::LessThan => write!(f, "<"),
            Token::LessThanOrEqualTo => write!(f, "<=>"),
            Token::Plus => write!(f, "+"),
            Token::Minus => write!(f, "-"),
            Token::Concatenation => write!(f, "<>"),
            Token::Times => write!(f, "*"),
            Token::Divide => write!(f, "/"),
            Token::Assign => write!(f, "="),
            Token::Pipe => write!(f, "|"),
            Token::DoubleColon => write!(f, "::"),
            Token::Dot => write!(f, "."),
            Token::Arrow => write!(f, "->"),
            Token::DoubleArrow => write!(f, "=>"),
            Token::Comma => write!(f, ","),
            Token::LeftParenthesis => write!(f, "("),
            Token::RightParenthesis => write!(f, ")"),
            Token::Eof => write!(f, "end of file"),
            Token::VirtualLeftBrace => write!(f, "{{"),
            Token::VirtualRightBrace => write!(f, "}}"),
            Token::VirtualSemicolon => write!(f, ";"),
        }
    }
}

pub fn match_lowercase(string: &str) -> Token {
    match string {
        "case" => Token::Case,
        "class" => Token::Class,
        "data" => Token::Data,
        "do" => Token::Do,
        "else" => Token::Else,
        "forall" => Token::ForAll,
        "if" => Token::If,
        "import" => Token::Import,
        "in" => Token::In,
        "instance" => Token::Instance,
        "let" => Token::Let,
        "module" => Token::Module,
        "of" => Token::Of,
        "then" => Token::Then,
        "where" => Token::Where,
        "false" => Token::Literal(Literal::Boolean(false)),
        "true" => Token::Literal(Literal::Boolean(true)),
        _ => Token::LIdent(string.to_owned())
    }
}
