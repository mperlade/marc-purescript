use crate::error::CompilerError;
use crate::parser::ast::Ast;
use crate::parser::token::Token;
use crate::parser::whitespace_lexer::WhitespaceLexer;
use lalrpop_util::ParseError;
use lexgen_util::{LexerError, Loc};

pub mod ast;
mod lexer;
pub mod token;
mod whitespace_lexer;

mod purescript {
    include!(concat!(env!("OUT_DIR"), "/parser/purescript.rs"));
}

#[derive(Debug)]
pub enum CustomParseError {
    LexerError(LexerError<String>),
    SyntaxError(Loc, Loc, String),
}

fn from_tokens(
    tokens: impl Iterator<Item = Result<(Loc, Token, Loc), LexerError<String>>>,
) -> Result<Ast, ParseError<Loc, Token, CustomParseError>> {
    let parser = purescript::FileParser::new();
    parser.parse(
        tokens
            .map(|result| result.map_err(|lexer_error| CustomParseError::LexerError(lexer_error))),
    )
}

pub fn parse(file_name: &str, code: &str) -> Result<Ast, CompilerError> {
    let lexer = WhitespaceLexer::new(code);
    from_tokens(lexer).map_err(|error| CompilerError::ParseError {
        file_name: file_name.to_owned(),
        error,
    })
}