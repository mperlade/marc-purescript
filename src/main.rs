extern crate core;

mod parser;
mod error;
mod typing;
mod codegen;

use std::{fs, panic};
use std::path::{Path, PathBuf};
use crate::codegen::{CallingConvention, gas_formatter};
use crate::error::CompilerError;

enum CompilerMode {
    ParseOnly,
    TypeOnly,
    Compile,
    CompileWindows,
}

fn parse_command_line() -> Result<(CompilerMode, String), CompilerError> {
    let mut args = std::env::args();
    args.next();
    let first_arg = args.next().ok_or(CompilerError::CommandLineError)?;
    let (mode, path) = match first_arg.as_str() {
        "--parse-only" => {
            (
                CompilerMode::ParseOnly,
                args.next().ok_or(CompilerError::CommandLineError)?,
            )
        }
        "--type-only" => {
            (
                CompilerMode::TypeOnly,
                args.next().ok_or(CompilerError::CommandLineError)?,
            )
        }
        "--windows" => {
            (
                CompilerMode::CompileWindows,
                args.next().ok_or(CompilerError::CommandLineError)?,
            )
        }
        _ => {
            (
                CompilerMode::Compile,
                first_arg,
            )
        }
    };
    if args.next().is_some() {
        Err(CompilerError::CommandLineError)
    } else {
        Ok((mode, path))
    }
}

fn load(path: &str) -> Result<(String, String), CompilerError> {
    let code = fs::read_to_string(path).map_err(|err| {
        CompilerError::FileError {
            path: path.to_owned(),
            error: err,
        }
    })?;

    let file_name = Path::new(path)
        .file_name()
        .unwrap() //Path should not end in .. if we successfully read the file
        .to_str()
        .unwrap() //Should be valid unicode
        .to_owned();

    Ok((file_name, code))
}

fn save(path: &str, compiled: &str) -> Result<(), CompilerError> {
    fs::write(path, compiled).map_err(|error| {
        CompilerError::SaveError(error)
    })
}

fn run_compiler() -> Result<(), CompilerError> {
    let (mode, path) = parse_command_line()?;
    let (file_name, code) = load(&path)?;
    match mode {
        CompilerMode::ParseOnly => {
            parser::parse(&file_name, &code)?;
        }
        CompilerMode::TypeOnly => {
            let ast = parser::parse(&file_name, &code)?;
            typing::r#type(&file_name, ast)?;
        }
        CompilerMode::Compile => {
            let mut output_path = PathBuf::from(&file_name);
            output_path.set_extension("s");
            let output_path = output_path.to_str().unwrap();

            let ast = parser::parse(&file_name, &code)?;
            let typed = typing::r#type(&file_name, ast)?;
            let compiled = codegen::compile_typed(&typed, CallingConvention::Sysv64);
            save(output_path, &gas_formatter::gas_format_asm(&compiled))?;
        }
        CompilerMode::CompileWindows => {
            let mut output_path = PathBuf::from(&file_name);
            output_path.set_extension("s");
            let output_path = output_path.to_str().unwrap();

            let ast = parser::parse(&file_name, &code)?;
            let typed = typing::r#type(&file_name, ast)?;
            let compiled = codegen::compile_typed(&typed, CallingConvention::Win64);
            save(output_path, &gas_formatter::gas_format_asm(&compiled))?;
        }
    }
    Ok(())
}

fn main() {
    let default_panic = panic::take_hook();
    panic::set_hook(Box::new(move |info| {
        default_panic(info);
        std::process::exit(2); //Exit with code 2 on panic
    }));

    let result = run_compiler();
    match result {
        Ok(()) => {
            std::process::exit(0);
        }
        Err(error) => {
            eprintln!("{}", error);
            std::process::exit(1);
        }
    }
}
