use crate::typing::typed_file::{Constant, InstanceResolution, Type, TypedData, TypedExpression, VariableIdentifier};
use std::collections::HashMap;
use crate::codegen::assembly::{Arg, AsmBuffer, Immediate, Reg};
use crate::codegen::case;
use crate::parser::ast::BinaryOperation;

pub struct ExpressionCompilationEnvironment<'a> {
    pub data: &'a [TypedData],
    pub mappings: &'a HashMap<VariableIdentifier, i64>,
    unique_label_prefix: &'a str,
    label_counter: u64,
    append_at_the_end: AsmBuffer,
}

impl<'a> ExpressionCompilationEnvironment<'a> {
    pub fn next(&mut self) -> String {
        let output = format!("{}_{}", self.unique_label_prefix, self.label_counter);
        self.label_counter += 1;
        output
    }

    pub fn string_literal(&mut self, value: &str) -> String {
        let label = self.next();
        self.append_at_the_end
            .label(&label)
            .string(value);
        label
    }
}

fn compile_resolution(resolution: &InstanceResolution) -> AsmBuffer {
    match resolution {
        InstanceResolution::InstanceVariable(index) => {
            let mut output = AsmBuffer::new();
            output
                .movq(Arg::IndDispBase(Immediate::Value(16), Reg::Rbp), Reg::Rax)
                .addq(8 + 8 * *index as i64, Reg::Rax)
                .pushq(Arg::IndBase(Reg::Rax));
            output
        }
        InstanceResolution::InstanceSchema(index, constraints) => {
            let mut output = AsmBuffer::new();
            for constraint in constraints.iter() {
                output.append(compile_resolution(constraint));
            }
            output
                .pushq(8 * (1 + constraints.len() as i64))
                .call("alloc")
                .addq(8, Reg::Rsp)
                .movq(*index as i64, Arg::IndBase(Reg::Rax));
            for i in 0..constraints.len() {
                let copied_address = 8 * (constraints.len() - 1 - i) as i64;
                let copy_to_address = 8 + 8 * i as i64;
                output
                    .movq(Arg::IndDispBase(Immediate::Value(copied_address), Reg::Rsp), Reg::Rcx)
                    .movq(Reg::Rcx, Arg::IndDispBase(Immediate::Value(copy_to_address), Reg::Rax));
            }
            output
                .addq(8 * constraints.len() as i64, Reg::Rsp)
                .pushq(Reg::Rax);
            output
        }
    }
}

fn compile_normal_binop(operation: BinaryOperation, left_type: &Type) -> AsmBuffer {
    //The first argument is on the stack
    //The second argument is in %rax
    let mut output = AsmBuffer::new();
    match operation {
        BinaryOperation::Equals => {
            match left_type {
                Type::Unit => {
                    output
                        .movb(1, Reg::Al)
                        .addq(8, Reg::Rsp);
                }
                Type::Boolean => {
                    output
                        .popq(Reg::Rcx)
                        .cmpb(Reg::Al, Reg::Cl)
                        .sete(Reg::Al);
                }
                Type::Int => {
                    output
                        .popq(Reg::Rcx)
                        .cmpl(Reg::Eax, Reg::Ecx)
                        .sete(Reg::Al);
                }
                Type::String => {
                    output
                        .pushq(Reg::Rax)
                        .call("string_comp")
                        .testl(Reg::Eax, Reg::Eax)
                        .sete(Reg::Al)
                        .addq(16, Reg::Rsp);
                },
                _ => unreachable!()
            }
        },
        BinaryOperation::NotEquals => {
            match left_type {
                Type::Unit => {
                    output
                        .movb(0, Reg::Al)
                        .addq(8, Reg::Rsp);
                }
                Type::Boolean => {
                    output
                        .popq(Reg::Rcx)
                        .cmpb(Reg::Al, Reg::Cl)
                        .setne(Reg::Al);
                }
                Type::Int => {
                    output
                        .popq(Reg::Rcx)
                        .cmpl(Reg::Eax, Reg::Ecx)
                        .setne(Reg::Al);
                }
                Type::String => {
                    output
                        .pushq(Reg::Rax)
                        .call("string_comp")
                        .testl(Reg::Eax, Reg::Eax)
                        .setne(Reg::Al)
                        .addq(16, Reg::Rsp);
                }
                _ => unreachable!()
            }
        },
        BinaryOperation::GreaterThan => {
            output
                .popq(Reg::Rcx)
                .cmpl(Reg::Eax, Reg::Ecx)
                .setg(Reg::Al);
        },
        BinaryOperation::GreaterThanOrEqualTo => {
            output
                .popq(Reg::Rcx)
                .cmpl(Reg::Eax, Reg::Ecx)
                .setge(Reg::Al);
        },
        BinaryOperation::LessThan => {
            output
                .popq(Reg::Rcx)
                .cmpl(Reg::Eax, Reg::Ecx)
                .setl(Reg::Al);
        }
        BinaryOperation::LessThanOrEqualTo => {
            output
                .popq(Reg::Rcx)
                .cmpl(Reg::Eax, Reg::Ecx)
                .setle(Reg::Al);
        },
        BinaryOperation::Plus => {
            output
                .popq(Reg::Rcx)
                .addl(Reg::Ecx, Reg::Eax);
        }
        BinaryOperation::Minus => {
            output
                .movq(Reg::Rax, Reg::Rcx)
                .popq(Reg::Rax)
                .subl(Reg::Ecx, Reg::Eax);
        }
        BinaryOperation::Concatenation => {
            output
                .pushq(Reg::Rax)
                .call("string_concat")
                .addq(16, Reg::Rsp);
        },
        BinaryOperation::Times => {
            output
                .popq(Reg::Rcx)
                .imull(Reg::Ecx, Reg::Eax);
        }
        BinaryOperation::Divide => {
            output
                .pushq(Reg::Rax)
                .call("div_euclid")
                .addq(16, Reg::Rsp);
        }
        _ => unreachable!()
    }
    output
}

pub fn compile_expression_recursive(
    environment: &mut ExpressionCompilationEnvironment,
    expression: &TypedExpression,
) -> AsmBuffer {
    match expression {
        TypedExpression::LocalVariable(_, variable) => {
            let mut output = AsmBuffer::new();
            let location = *environment.mappings.get(variable).unwrap();
            output.movq(Arg::IndDispBase(Immediate::Value(location), Reg::Rbp), Reg::Rax);
            output
        }
        TypedExpression::Log(_, expression) => {
            let mut output = compile_expression_recursive(
                environment,
                expression,
            );
            output
                .pushq(Reg::Rax)
                .call("log")
                .addq(8, Reg::Rsp);
            output
        }
        TypedExpression::Constant(_, constant) => match constant {
            Constant::Integer(integer) => {
                let mut output = AsmBuffer::new();
                output.movl(*integer as i64, Reg::Eax);
                output
            }
            Constant::String(string) => {
                let label = environment.string_literal(string);
                let mut output = AsmBuffer::new();
                output.movq(label.as_str(), Reg::Rax);
                output
            }
            Constant::Boolean(bit) => {
                let mut output = AsmBuffer::new();
                if *bit {
                    output.movb(1, Reg::Al);
                } else {
                    output.movb(0, Reg::Al);
                }
                output
            }
        }
        TypedExpression::Application(_, resolution, identifier, expressions) => {
            let mut output = AsmBuffer::new();
            for expression in expressions.iter() {
                output.append(compile_expression_recursive(
                    environment,
                    expression,
                ));
                output.pushq(Reg::Rax);
            }
            output.append(compile_resolution(resolution));
            output
                .call(&format!("F_{identifier}"))
                .addq(8 * (1 + expressions.len() as i64), Reg::Rsp);
            output
        }
        TypedExpression::Case(_, expression, branches) => {
            case::compile_pattern_matching(environment, expression, branches)
        }
        TypedExpression::Let(_, assignments, expression) => {
            let mut output = AsmBuffer::new();
            for (identifier, expression) in assignments {
                let location = *environment.mappings.get(identifier).unwrap();
                output.append(compile_expression_recursive(environment, expression));
                output.movq(Reg::Rax, Arg::IndDispBase(Immediate::Value(location), Reg::Rbp));
            }
            output.append(compile_expression_recursive(environment, expression));
            output
        }
        TypedExpression::IntToString(_, expression) => {
            let mut output = compile_expression_recursive(
                environment,
                expression,
            );
            output
                .pushq(Reg::Rax)
                .call("int_to_string")
                .addq(8, Reg::Rsp);
            output
        }
        TypedExpression::UnaryMinus(_, expression) => {
            let mut output = compile_expression_recursive(
                environment,
                expression,
            );
            output.negl(Reg::Eax);
            output
        },
        TypedExpression::BinaryOperation(_, a, op, b) => {
            match op {
                BinaryOperation::LogicalOr => {
                    let skip_label = environment.next();
                    let mut output = compile_expression_recursive(
                        environment,
                        &*a,
                    );
                    output
                        .testb(Reg::Al, Reg::Al)
                        .jne(&skip_label);
                    output.append(compile_expression_recursive(
                        environment,
                        &*b,
                    ));
                    output.label(&skip_label);
                    output
                },
                BinaryOperation::LogicalAnd => {
                    let skip_label = environment.next();
                    let mut output = compile_expression_recursive(
                        environment,
                        &*a,
                    );
                    output
                        .testb(Reg::Al, Reg::Al)
                        .je(&skip_label);
                    output.append(compile_expression_recursive(
                        environment,
                        &*b,
                    ));
                    output.label(&skip_label);
                    output
                },
                _ => {
                    let mut output = compile_expression_recursive(
                        environment,
                        &*a,
                    );
                    output.pushq(Reg::Rax);
                    output.append(compile_expression_recursive(
                        environment,
                        &*b,
                    ));
                    output.append(compile_normal_binop(*op, a.get_type()));
                    output
                }
            }
        },
        TypedExpression::Constructor(_, _, variant, expressions) => {
            let mut output = AsmBuffer::new();
            for expression in expressions {
                output.append(compile_expression_recursive(
                    environment,
                    expression
                ));
                output.pushq(Reg::Rax);
            }
            output
                .pushq(8 * (1 + expressions.len() as i64))
                .call("alloc")
                .addq(8, Reg::Rsp)
                .movq(*variant as i64, Arg::IndBase(Reg::Rax));
            for i in 0..expressions.len() {
                let copied_address = 8 * (expressions.len() - 1 - i) as i64;
                let copy_to_address = 8 + 8 * i as i64;

                output
                    .movq(Arg::IndDispBase(Immediate::Value(copied_address), Reg::Rsp), Reg::Rcx)
                    .movq(Reg::Rcx, Arg::IndDispBase(Immediate::Value(copy_to_address), Reg::Rax));
            }
            output
                .addq(8 * expressions.len() as i64, Reg::Rsp);
            output
        },
        TypedExpression::IfThenElse(
            _,
            condition,
            branch_true,
            branch_false
        ) => {
            let mut output = compile_expression_recursive(
                environment,
                &*condition
            );
            let label_false = environment.next();
            let end_label = environment.next();
            output
                .testb(Reg::Al, Reg::Al)
                .je(&label_false);
            output.append(compile_expression_recursive(
                environment,
                &*branch_true
            ));
            output
                .jmp(&end_label)
                .label(&label_false);
            output.append(compile_expression_recursive(
                environment,
                &*branch_false
            ));
            output.label(&end_label);
            output
        },
        TypedExpression::Do(_, expressions) => {
            let mut output = AsmBuffer::new();
            for expression in expressions.iter() {
                output.append(compile_expression_recursive(
                    environment,
                    expression,
                ));
            }
            output
        },
        TypedExpression::Not(_, expression) => {
            let mut output = compile_expression_recursive(
                environment,
                expression,
            );
            output
                .testb(Reg::Al, Reg::Al)
                .sete(Reg::Al);
            output
        },
        TypedExpression::Mod(_, a, b) => {
            let mut output = compile_expression_recursive(
                environment,
                &*a,
            );
            output.pushq(Reg::Rax);
            output.append(compile_expression_recursive(
                environment,
                &*b,
            ));
            output
                .pushq(Reg::Rax)
                .call("rem_euclid")
                .addq(16, Reg::Rsp);
            output
        },
        TypedExpression::Pure(_, expression) => {
            compile_expression_recursive(
                environment,
                &*expression,
            )
        },
        TypedExpression::Unit(_) => AsmBuffer::new(),
    }
}

pub fn compile_expression(
    data: &[TypedData],
    unique_label_prefix: &str,
    expression: &TypedExpression,
    mappings: &HashMap<VariableIdentifier, i64>,
) -> (AsmBuffer, AsmBuffer) {
    let mut environment = ExpressionCompilationEnvironment {
        data,
        mappings,
        unique_label_prefix,
        label_counter: 0,
        append_at_the_end: AsmBuffer::new(),
    };
    (
        compile_expression_recursive(
            &mut environment,
            expression,
        ),
        environment.append_at_the_end
    )
}
