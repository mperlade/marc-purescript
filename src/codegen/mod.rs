use crate::codegen::assembly::{Arg, AsmBuffer, Immediate, Reg};
use crate::typing::typed_file::{FunctionBody, FunctionIdentifier, InstanceIdentifier, TypedData, TypedFile, TypedFunction};

mod stack_frame;
mod expression;
mod case;

#[allow(dead_code)]
pub mod assembly;
pub mod gas_formatter;
mod sysv64_prelude;
mod win64_prelude;

pub enum CallingConvention {
    Sysv64,
    Win64,
}

fn compile_function_body(
    data: &[TypedData],
    body: &FunctionBody,
    identifier: FunctionIdentifier,
    index: InstanceIdentifier
) -> AsmBuffer {
    let (stack_frame_size, mappings) = stack_frame::allocate_expression(&body.expression);

    let mappings = mappings
        .iter()
        .map(|(variable, location)| {
            (*variable, -8 - 8 * (*location as i64))
        })
        .chain(body.arguments
            .iter()
            .rev() //First argument is highest on the stack
            .enumerate()
            .map(|(index, argument)| {
                (*argument, 24 + 8 * (index as i64))
            }))
        .collect();

    let prefix = format!("F_{identifier}_B_{index}");

    let (middle, end) = expression::compile_expression(
        data,
        &prefix,
        &body.expression,
        &mappings
    );

    let mut output = AsmBuffer::new();
    output
        .label(&prefix)
        .pushq(Reg::Rbp)
        .movq(Reg::Rsp, Reg::Rbp)
        .subq(stack_frame_size as i64 * 8, Reg::Rsp);

    output.append(middle);

    output
        .movq(Reg::Rbp, Reg::Rsp)
        .popq(Reg::Rbp)
        .ret();

    output.append(end);

    output
}

fn compile_function(data: &[TypedData], function: &TypedFunction, identifier: FunctionIdentifier) -> AsmBuffer {
    // Stack when a function is called:
    //
    // --                   <-- %rbp
    // --
    // --
    // argument 1
    // .
    // .
    // argument n           <-- %rsp + 16
    // resolution           <-- %rsp + 8
    // return address       <-- %rsp

    let jump_table_label = format!("F_{identifier}_jmptbl");

    let mut output = AsmBuffer::new();
    output
        .label(&format!("F_{identifier}"))
        .movq(Arg::IndDispBase(Immediate::Value(8), Reg::Rsp), Reg::Rax)
        .movq(Arg::IndBase(Reg::Rax), Reg::Rax)
        .jmp_star(Arg::IndDispIdxScale(Immediate::Label(jump_table_label.clone()), Reg::Rax, 8));

    output
        .label(&jump_table_label);

    for body_index in 0..function.bodies.len() {
        output.quad(Immediate::Label(format!("F_{identifier}_B_{body_index}")));
    }

    for (index, body) in function.bodies.iter().enumerate() {
        output.append(compile_function_body(data, body, identifier, index));
    }

    output
}

pub fn compile_typed(typed_file: &TypedFile, calling_convention: CallingConvention) -> AsmBuffer  {
    let mut output = match calling_convention {
        CallingConvention::Sysv64 => sysv64_prelude::prelude(),
        CallingConvention::Win64 => win64_prelude::prelude(),
    };

    output
        .label("main")
        .pushq("main_resolution")
        .call(&format!("F_{}", typed_file.main))
        .addq(8, Reg::Rsp)
        .xorq(Reg::Rax, Reg::Rax)
        .ret();
    output
        .label("main_resolution")
        .quad(Immediate::Value(0));

    for (identifier, function) in typed_file.functions.iter().enumerate() {
        output.append(compile_function(&typed_file.data, function, identifier));
    }

    output
}
