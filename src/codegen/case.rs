use crate::codegen::expression;
use crate::codegen::expression::ExpressionCompilationEnvironment;
use crate::typing::typed_file::{Constant, TypedExpression, TypedPattern, VariantIndex};
use std::collections::HashSet;
use ndarray::{Array2, ArrayView2, Axis};
use crate::codegen::assembly::{Arg, AsmBuffer, Immediate, Reg};

#[derive(Clone)]
struct JumpInfo<'a> {
    move_before_jump: Vec<(u64, i64)>, //movq x(%rsp), y(%rbp)
    deallocate_stack: u64,
    jump_label: &'a str,
}

impl<'a> JumpInfo<'a> {
    fn compile(&self) -> AsmBuffer {
        let mut output = AsmBuffer::new();
        for (source, destination) in self.move_before_jump.iter() {
            let source_location = 8 * *source as i64;
            output
                .movq(Arg::IndDispBase(Immediate::Value(source_location), Reg::Rsp), Reg::Rax)
                .movq(Reg::Rax, Arg::IndDispBase(Immediate::Value(*destination), Reg::Rbp));
        }
        output
            .addq(8 * self.deallocate_stack as i64, Reg::Rsp)
            .jmp(self.jump_label);
        output
    }
}

fn compile_first_column_constant(
    environment: &mut ExpressionCompilationEnvironment,
    matrix: ArrayView2<Option<&TypedPattern>>,
    matched: &[u64],
    jump_info: &[JumpInfo],
    constant: Option<Constant>,
) -> AsmBuffer {
    let shape = matrix.shape();
    let new_matched = &matched[1..];

    let mut flattened_new_matrix: Vec<Option<&TypedPattern>> = Vec::new();
    let mut new_jump_info: Vec<JumpInfo> = Vec::new();

    for (row, jump_info) in matrix
        .axis_iter(Axis(0))
        .zip(jump_info.iter())
    {
        match &row[0] {
            None => {
                flattened_new_matrix.extend(row.slice(ndarray::s![1..]).iter());
                new_jump_info.push(jump_info.clone());
            }
            Some(TypedPattern::Variable(_, identifier)) => {
                flattened_new_matrix.extend(row.slice(ndarray::s![1..]).iter());
                let mut new_move = jump_info.move_before_jump.clone();
                let move_to_address = environment.mappings.get(identifier).unwrap();
                new_move.push((matched[0], *move_to_address));
                new_jump_info.push(JumpInfo {
                    move_before_jump: new_move,
                    deallocate_stack: jump_info.deallocate_stack,
                    jump_label: jump_info.jump_label,
                });
            }
            Some(TypedPattern::Constant(_, value)) => {
                if let Some(constant) = constant.as_ref() {
                    if *value == *constant {
                        flattened_new_matrix.extend(row.slice(ndarray::s![1..]).iter());
                        new_jump_info.push(jump_info.clone());
                    }
                }
            }
            _ => {}
        }
    }

    let new_shape = (new_jump_info.len(), shape[1] - 1);
    let new_matrix =
        Array2::from_shape_vec(new_shape, flattened_new_matrix)
            .unwrap();

    compile_recursive(environment, new_matrix.view(), new_matched, &new_jump_info)
}

fn compile_first_column_booleans(
    environment: &mut ExpressionCompilationEnvironment,
    matrix: ArrayView2<Option<&TypedPattern>>,
    matched: &[u64],
    jump_info: &[JumpInfo],
) -> AsmBuffer {
    let compiled_false = compile_first_column_constant(
        environment,
        matrix,
        matched,
        jump_info,
        Some(Constant::Boolean(false)),
    );

    let compiled_true = compile_first_column_constant(
        environment,
        matrix,
        matched,
        jump_info,
        Some(Constant::Boolean(true)),
    );

    let boolean_location = 8 * matched[0] as i64;
    let true_label = environment.next();
    let mut output = AsmBuffer::new();
    output
        .cmpb(0, Arg::IndDispBase(Immediate::Value(boolean_location), Reg::Rsp))
        .jne(&true_label);
    output.append(compiled_false);
    output.label(&true_label);
    output.append(compiled_true);
    output
}

fn compile_first_column_ints(
    environment: &mut ExpressionCompilationEnvironment,
    matrix: ArrayView2<Option<&TypedPattern>>,
    matched: &[u64],
    jump_info: &[JumpInfo],
) -> AsmBuffer {
    let mut involved = matrix.slice(ndarray::s![.., 0])
        .iter()
        .filter_map(|pattern| {
            match pattern {
                Some(TypedPattern::Constant(_, Constant::Integer(value))) => Some(*value),
                Some(TypedPattern::Variable(_, _)) | None => None,
                _ => unreachable!()
            }
        })
        .collect::<Vec<i32>>();
    involved.sort();
    involved.dedup();

    let int_location = 8 * matched[0] as i64;
    let mut output = AsmBuffer::new();
    let mut after = AsmBuffer::new();
    for integer in involved.iter() {
        let label = environment.next();
        output
            .cmpl(*integer as i64, Arg::IndDispBase(Immediate::Value(int_location), Reg::Rsp))
            .je(&label);
        after.label(&label);
        after.append(compile_first_column_constant(
            environment,
            matrix,
            matched,
            jump_info,
            Some(Constant::Integer(*integer))
        ))
    }
    output.append(compile_first_column_constant(
        environment,
        matrix,
        matched,
        jump_info,
        None
    )); //default case
    output.append(after);
    output
}

fn compile_first_column_strings(
    environment: &mut ExpressionCompilationEnvironment,
    matrix: ArrayView2<Option<&TypedPattern>>,
    matched: &[u64],
    jump_info: &[JumpInfo],
) -> AsmBuffer {
    let mut involved = matrix.slice(ndarray::s![.., 0])
        .iter()
        .filter_map(|pattern| {
            match pattern {
                Some(TypedPattern::Constant(_, Constant::String(value))) => Some(value.as_str()),
                Some(TypedPattern::Variable(_, _)) | None => None,
                _ => unreachable!()
            }
        })
        .collect::<Vec<&str>>();
    involved.sort();
    involved.dedup();

    let string_location = 8 * matched[0] as i64;
    let mut output = AsmBuffer::new();
    let mut after = AsmBuffer::new();
    for string in involved.iter() {
        let label = environment.next();
        let string_label = environment.string_literal(*string);
        output
            .pushq(Arg::IndDispBase(Immediate::Value(string_location), Reg::Rsp))
            .pushq(string_label.as_str())
            .call("string_comp")
            .addq(16, Reg::Rsp)
            .testl(Reg::Eax, Reg::Eax)
            .je(&label);
        after.label(&label);
        after.append(compile_first_column_constant(
            environment,
            matrix,
            matched,
            jump_info,
            Some(Constant::String((*string).to_owned()))
        ))
    }
    output.append(compile_first_column_constant(
        environment,
        matrix,
        matched,
        jump_info,
        None
    )); //default case
    output.append(after);
    output
}

fn jump_info_allocate_stack(jump_info: &mut JumpInfo, slots: u64) {
    jump_info.deallocate_stack += slots;
    for (from_where, _) in jump_info.move_before_jump.iter_mut() {
        *from_where += slots;
    }
}

fn compile_first_column_constructor_variant(
    environment: &mut ExpressionCompilationEnvironment,
    matrix: ArrayView2<Option<&TypedPattern>>,
    matched: &[u64],
    jump_info: &[JumpInfo],
    variant: VariantIndex,
    variant_arity: usize,
) -> AsmBuffer {
    let shape = matrix.shape();

    let new_matched = (0..variant_arity)
        .map(|index| {
            (variant_arity - 1 - index) as u64
        })
        .chain(matched[1..]
            .iter()
            .map(|position| {
                //Other variables are now further up the stack
                *position + variant_arity as u64
            })
        )
        .collect::<Vec<u64>>();

    let mut flattened_new_matrix: Vec<Option<&TypedPattern>> = Vec::new();
    let mut new_jump_info: Vec<JumpInfo> = Vec::new();

    for (row, jump_info) in matrix
        .axis_iter(Axis(0))
        .zip(jump_info.iter())
    {
        match &row[0] {
            None => {
                for _ in 0..variant_arity {
                    flattened_new_matrix.push(None);
                }
                flattened_new_matrix.extend(row.slice(ndarray::s![1..]).iter());
                new_jump_info.push(jump_info.clone());
            }
            Some(TypedPattern::Variable(_, identifier)) => {
                for _ in 0..variant_arity {
                    flattened_new_matrix.push(None);
                }
                flattened_new_matrix.extend(row.slice(ndarray::s![1..]).iter());
                let mut new_move = jump_info.move_before_jump.clone();
                let move_to_address = environment.mappings.get(identifier).unwrap();
                new_move.push((matched[0], *move_to_address));
                new_jump_info.push(JumpInfo {
                    move_before_jump: new_move,
                    deallocate_stack: jump_info.deallocate_stack,
                    jump_label: jump_info.jump_label,
                });
            }
            Some(TypedPattern::Constructor(_, _, index, patterns)) => {
                if *index == variant {
                    flattened_new_matrix.extend(patterns.iter().map(|pattern| Some(pattern)));
                    flattened_new_matrix.extend(row.slice(ndarray::s![1..]).iter());
                    new_jump_info.push(jump_info.clone());
                }
            }
            _ => {}
        }
    }

    let new_shape = (new_jump_info.len(), shape[1] - 1 + variant_arity);
    let new_matrix =
        Array2::from_shape_vec(new_shape, flattened_new_matrix)
            .unwrap();

    //Push fields on the stack
    let pointer_location = 8 * matched[0] as i64;
    let mut output = AsmBuffer::new();
    output.movq(Arg::IndDispBase(Immediate::Value(pointer_location), Reg::Rsp), Reg::Rax);
    for index in 0..variant_arity {
        let location = 8 + 8 * index as i64;
        output.pushq(Arg::IndDispBase(Immediate::Value(location), Reg::Rax));
    }

    //Update jump info
    for new_jump_info in new_jump_info.iter_mut() {
        jump_info_allocate_stack(new_jump_info, variant_arity as u64);
    }

    output.append(compile_recursive(environment, new_matrix.view(), &new_matched, &new_jump_info));
    output
}

fn compile_first_column_constructors(
    environment: &mut ExpressionCompilationEnvironment,
    matrix: ArrayView2<Option<&TypedPattern>>,
    matched: &[u64],
    jump_info: &[JumpInfo],
) -> AsmBuffer {
    let mut data_identifier = None;
    let involved_variants = matrix.slice(ndarray::s![.., 0])
        .iter()
        .filter_map(|pattern| {
            match pattern {
                Some(TypedPattern::Constructor(_, data, variant, _)) => {
                    data_identifier = Some(*data);
                    Some(*variant)
                },
                Some(TypedPattern::Variable(_, _)) | None => None,
                _ => unreachable!()
            }
        })
        .collect::<HashSet<VariantIndex>>();
    let data_identifier = data_identifier.unwrap();

    let jump_table_label = environment.next();
    let pointer_location = 8 * matched[0] as i64;
    let mut output = AsmBuffer::new();
    output
        .movq(Arg::IndDispBase(Immediate::Value(pointer_location), Reg::Rsp), Reg::Rax)
        .movq(Arg::IndBase(Reg::Rax), Reg::Rax)
        .jmp_star(Arg::IndDispIdxScale(Immediate::Label(jump_table_label.clone()), Reg::Rax, 8))
        .label(&jump_table_label);

    let mut after = AsmBuffer::new();

    let default_label = environment.next();
    let mut has_default = false;
    for (variant_index, variant) in environment
        .data[data_identifier]
        .variants
        .iter()
        .enumerate()
    {
        if involved_variants.contains(&variant_index) {
            let label = environment.next();
            output.quad(Immediate::Label(label.clone()));
            after.label(&label);
            after.append(compile_first_column_constructor_variant(
                environment,
                matrix,
                matched,
                jump_info,
                variant_index,
                variant.len(),
            ));
        } else {
            output.quad(Immediate::Label(default_label.clone()));
            has_default = true;
        }
    }

    after.label(&default_label);
    if has_default {
        after.append(compile_first_column_constant(
            environment,
            matrix,
            matched,
            jump_info,
            None
        ))
    }

    output.append(after);
    output
}

fn compile_recursive(
    environment: &mut ExpressionCompilationEnvironment,
    matrix: ArrayView2<Option<&TypedPattern>>,
    matched: &[u64], //column headers
    jump_info: &[JumpInfo], //row headers
) -> AsmBuffer {
    let shape = matrix.shape();
    if shape[0] == 0 {
        panic!("Pattern matching is not exhaustive! ");
    }
    if shape[1] == 0 {
        return jump_info[0].compile();
    }

    let not_variable = matrix
        .slice(ndarray::s![.., 0])
        .iter()
        .find_map(|pattern| {
            match pattern {
                Some(TypedPattern::Variable(_, _)) | None => None,
                Some(pattern) => Some(*pattern),
            }
        });

    if let Some(not_variable) = not_variable {
        match not_variable {
            TypedPattern::Constant(_, constant) => {
                match constant {
                    Constant::Integer(_) => {
                        compile_first_column_ints(
                            environment,
                            matrix,
                            matched,
                            jump_info,
                        )
                    }
                    Constant::String(_) => {
                        compile_first_column_strings(
                            environment,
                            matrix,
                            matched,
                            jump_info,
                        )
                    },
                    Constant::Boolean(_) => {
                        compile_first_column_booleans(
                            environment,
                            matrix,
                            matched,
                            jump_info,
                        )
                    }
                }
            }
            TypedPattern::Constructor(_, _, _, _) => {
                compile_first_column_constructors(
                    environment,
                    matrix,
                    matched,
                    jump_info
                )
            },
            _ => unreachable!()
        }
    } else {
        //The first column is only variables
        let new_matrix = matrix.slice(ndarray::s![.., 1..]);
        let new_matched = &matched[1..];
        let new_jump_info = jump_info
            .iter()
            .zip(matrix.slice(ndarray::s![.., 0]).iter())
            .map(|(jump_info, variable)| {
                if let Some(pattern) = variable {
                    if let TypedPattern::Variable(_, identifier) = *pattern {
                        let mut new_move = jump_info.move_before_jump.clone();
                        let move_to_address = environment.mappings.get(identifier).unwrap();
                        new_move.push((matched[0], *move_to_address));
                        JumpInfo {
                            move_before_jump: new_move,
                            deallocate_stack: jump_info.deallocate_stack,
                            jump_label: jump_info.jump_label,
                        }
                    } else {
                        unreachable!();
                    }
                } else {
                    jump_info.clone()
                }
            })
            .collect::<Vec<JumpInfo>>();
        compile_recursive(environment, new_matrix, new_matched, &new_jump_info)
    }
}

pub fn compile_pattern_matching(
    environment: &mut ExpressionCompilationEnvironment,
    expression: &TypedExpression,
    branches: &[(TypedPattern, TypedExpression)]
) -> AsmBuffer {
    let mut output = expression::compile_expression_recursive(environment, expression);
    //Result is in %rax
    output.pushq(Reg::Rax);
    //Pattern matching expects matched variables to be on the stack

    //First compile the expressions (to know where to jump)
    let end_label = environment.next();
    let mut expressions = AsmBuffer::new();
    let expression_labels = branches
        .iter()
        .map(|(_, expression)| {
            let label = environment.next();
            expressions.label(&label);
            expressions.append(expression::compile_expression_recursive(
                environment,
                expression,
            ));
            expressions.jmp(&end_label);
            label
        })
        .collect::<Vec<String>>();
    expressions.label(&end_label);

    let flattened_column: Vec<Option<&TypedPattern>> = branches.iter().map(|(pattern, _)| Some(pattern)).collect();
    let matrix = Array2::from_shape_vec((flattened_column.len(), 1), flattened_column).unwrap();
    let matched = [0u64]; //We match the top of the stack
    let jump_info = expression_labels
        .iter()
        .map(|label| {
            JumpInfo {
                move_before_jump: vec![],
                deallocate_stack: 1,
                jump_label: label.as_str(),
            }
        })
        .collect::<Vec<JumpInfo>>();
    output.append(compile_recursive(environment, matrix.view(), &matched, &jump_info));

    output.append(expressions);

    output
}