use crate::typing::typed_file::{TypedExpression, TypedPattern, VariableIdentifier};
use std::collections::HashMap;

fn allocate_variable(
    identifier: VariableIdentifier,
    stack_frame_size: &mut u64,
    mappings: &mut HashMap<VariableIdentifier, u64>,
) {
    mappings.insert(identifier, *stack_frame_size);
    *stack_frame_size += 1;
}

fn allocate_pattern_variables(
    pattern: &TypedPattern,
    stack_frame_size: &mut u64,
    mappings: &mut HashMap<VariableIdentifier, u64>,
) {
    match pattern {
        TypedPattern::Variable(_, variable) => {
            allocate_variable(*variable, stack_frame_size, mappings);
        }
        TypedPattern::Constructor(_, _, _, patterns) => {
            for pattern in patterns.iter() {
                allocate_pattern_variables(pattern, stack_frame_size, mappings);
            }
        }
        _ => {}
    }
}

fn allocate_expression_recursive(
    expression: &TypedExpression,
    stack_frame_size: u64,
    mappings: &mut HashMap<VariableIdentifier, u64>,
) -> u64 {
    match expression {
        TypedExpression::Let(_, assignments, expression) => {
            let mut current_size = stack_frame_size;
            let mut current_max = stack_frame_size;
            for (variable, assignment) in assignments.iter() {
                current_max = current_max.max(allocate_expression_recursive(
                    assignment,
                    current_size,
                    mappings,
                ));
                allocate_variable(*variable, &mut current_size, mappings);
            }
            current_max = current_max.max(allocate_expression_recursive(
                expression,
                current_size,
                mappings,
            ));
            current_max
        }
        TypedExpression::Case(_, expression, branches) => {
            let mut current_max = allocate_expression_recursive(expression, stack_frame_size, mappings);
            for (pattern, expression) in branches.iter() {
                let mut current_size = stack_frame_size;
                allocate_pattern_variables(pattern, &mut current_size, mappings);
                current_max = current_max.max(allocate_expression_recursive(
                    expression,
                    current_size,
                    mappings
                ));
            }
            current_max
        },

        TypedExpression::Constructor(_, _, _, subexpressions)
        | TypedExpression::Application(_, _, _, subexpressions)
        | TypedExpression::Do(_, subexpressions) => subexpressions
            .iter()
            .map(|expression| allocate_expression_recursive(expression, stack_frame_size, mappings))
            .max()
            .unwrap_or(stack_frame_size),
        TypedExpression::UnaryMinus(_, expression)
        | TypedExpression::Not(_, expression)
        | TypedExpression::Log(_, expression)
        | TypedExpression::Pure(_, expression)
        | TypedExpression::IntToString(_, expression) => {
            allocate_expression_recursive(&*expression, stack_frame_size, mappings)
        }
        TypedExpression::BinaryOperation(_, a, _, b) | TypedExpression::Mod(_, a, b) => u64::max(
            allocate_expression_recursive(&*a, stack_frame_size, mappings),
            allocate_expression_recursive(&*b, stack_frame_size, mappings),
        ),
        TypedExpression::IfThenElse(_, a, b, c) => [
            allocate_expression_recursive(&*a, stack_frame_size, mappings),
            allocate_expression_recursive(&*b, stack_frame_size, mappings),
            allocate_expression_recursive(&*c, stack_frame_size, mappings),
        ]
        .into_iter()
        .max()
        .unwrap(),
        _ => stack_frame_size,
    }
}

pub fn allocate_expression(
    expression: &TypedExpression,
) -> (u64, HashMap<VariableIdentifier, u64>) {
    let mut mappings = HashMap::new();
    let stack_frame_size = allocate_expression_recursive(expression, 0, &mut mappings);
    (stack_frame_size, mappings)
}
