use crate::codegen::assembly::{Arg, AsmBuffer, Immediate, Reg};

pub fn prelude() -> AsmBuffer {
    let mut output = AsmBuffer::new();

    output
        .label("log")
        .pushq(Reg::Rbp)
        .movq(Reg::Rsp, Reg::Rbp)
        .andq(-16, Reg::Rsp)
        .movq("log_format", Reg::Rcx)
        .movq(Arg::IndDispBase(Immediate::Value(16), Reg::Rbp), Reg::Rdx)
        .subq(32, Reg::Rsp)
        .call_printf()
        .movq(Reg::Rbp, Reg::Rsp)
        .popq(Reg::Rbp)
        .ret();
    output
        .label("log_format")
        .string("%s\n");

    output
        .label("alloc")
        .pushq(Reg::Rbp)
        .movq(Reg::Rsp, Reg::Rbp)
        .andq(-16, Reg::Rsp)
        .movq(Arg::IndDispBase(Immediate::Value(16), Reg::Rbp), Reg::Rcx)
        .subq(32, Reg::Rsp)
        .call_malloc()
        .movq(Reg::Rbp, Reg::Rsp)
        .popq(Reg::Rbp)
        .ret();

    output
        .label("int_to_string")
        .pushq(Reg::Rbp)
        .movq(Reg::Rsp, Reg::Rbp)
        .andq(-16, Reg::Rsp)
        .movq(12, Reg::Rcx)
        .subq(32, Reg::Rsp)
        .call_malloc()
        .movq(Reg::Rax, Reg::Rdi) //%rdi is callee-saved here
        .movq(Reg::Rdi, Reg::Rcx)
        .movq("sprintf_format", Reg::Rdx)
        .movq(Arg::IndDispBase(Immediate::Value(16), Reg::Rbp), Reg::R8)
        .call_sprintf()
        .movq(Reg::Rdi, Reg::Rax)
        .movq(Reg::Rbp, Reg::Rsp)
        .popq(Reg::Rbp)
        .ret();
    output
        .label("sprintf_format")
        .string("%d");

    output
        .label("div_euclid")
        .movl(Arg::IndDispBase(Immediate::Value(16), Reg::Rsp), Reg::Eax)
        .cdq()
        .movl(Arg::IndDispBase(Immediate::Value(8), Reg::Rsp), Reg::Ecx)
        .idivl(Reg::Ecx)
        .testl(Reg::Edx, Reg::Edx)
        .jns("div_euclid_end")
        .testl(Reg::Ecx, Reg::Ecx)
        .js("div_euclid_add_one")
        .decl(Reg::Eax)
        .ret();
    output
        .label("div_euclid_add_one")
        .incl(Reg::Eax)
        .ret();
    output
        .label("div_euclid_end")
        .ret();

    output
        .label("rem_euclid")
        .movl(Arg::IndDispBase(Immediate::Value(16), Reg::Rsp), Reg::Eax)
        .cdq()
        .movl(Arg::IndDispBase(Immediate::Value(8), Reg::Rsp), Reg::Ecx)
        .idivl(Reg::Ecx)
        .movl(Reg::Edx, Reg::Eax)
        .testl(Reg::Eax, Reg::Eax)
        .jns("rem_euclid_end")
        .testl(Reg::Ecx, Reg::Ecx)
        .jns("rem_euclid_add_divisor")
        .subl(Reg::Ecx, Reg::Eax)
        .ret();
    output
        .label("rem_euclid_add_divisor")
        .addl(Reg::Ecx, Reg::Eax)
        .ret();
    output
        .label("rem_euclid_end")
        .ret();

    output
        .label("string_comp")
        .pushq(Reg::Rbp)
        .movq(Reg::Rsp, Reg::Rbp)
        .andq(-16, Reg::Rsp)
        .movq(Arg::IndDispBase(Immediate::Value(24), Reg::Rbp), Reg::Rcx)
        .movq(Arg::IndDispBase(Immediate::Value(16), Reg::Rbp), Reg::Rdx)
        .subq(32, Reg::Rsp)
        .call_strcmp()
        .movq(Reg::Rbp, Reg::Rsp)
        .popq(Reg::Rbp)
        .ret();

    output
        .label("string_concat")
        .pushq(Reg::Rbp)
        .movq(Reg::Rsp, Reg::Rbp)
        .andq(-16, Reg::Rsp)
        .subq(32, Reg::Rsp)
        .movq(Arg::IndDispBase(Immediate::Value(24), Reg::Rbp), Reg::Rcx)
        .call_strlen()
        .movq(Reg::Rax, Reg::Rdi)
        .movq(Arg::IndDispBase(Immediate::Value(16), Reg::Rbp), Reg::Rcx)
        .call_strlen()
        .addq(Reg::Rdi, Reg::Rax)
        .incq(Reg::Rax)
        .movq(Reg::Rax, Reg::Rcx)
        .call_malloc()
        .movq(Reg::Rax, Reg::Rdi)
        .movq(Reg::Rdi, Reg::Rcx)
        .movq(Arg::IndDispBase(Immediate::Value(24), Reg::Rbp), Reg::Rdx)
        .call_strcpy()
        .movq(Reg::Rdi, Reg::Rcx)
        .movq(Arg::IndDispBase(Immediate::Value(16), Reg::Rbp), Reg::Rdx)
        .call_strcat()
        .movq(Reg::Rdi, Reg::Rax)
        .movq(Reg::Rbp, Reg::Rsp)
        .popq(Reg::Rbp)
        .ret();

    output
}