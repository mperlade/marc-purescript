pub enum Reg {
    Rax,
    Rbx,
    Rcx,
    Rdx,
    Rsi,
    Rdi,
    Rbp,
    Rsp,
    R8,
    R9,
    R10,
    R11,
    R12,
    R13,
    R14,
    R15,

    Eax,
    Ebx,
    Ecx,
    Edx,
    Esi,
    Edi,
    Ebp,
    Esp,
    R8d,
    R9d,
    R10d,
    R11d,
    R12d,
    R13d,
    R14d,
    R15d,

    Ax,
    Bx,
    Cx,
    Dx,
    Si,
    Di,
    Bp,
    Sp,
    R8w,
    R9w,
    R10w,
    R11w,
    R12w,
    R13w,
    R14w,
    R15w,

    Al,
    Bl,
    Cl,
    Dl,
    Ah,
    Bh,
    Ch,
    Dh,
    Sil,
    Dil,
    Bpl,
    Spl,
    R8b,
    R9b,
    R10b,
    R11b,
    R12b,
    R13b,
    R14b,
    R15b,
}

pub enum Immediate {
    Value(i64),
    Label(String),
}

pub enum Arg {
    Register(Reg),
    Immediate(Immediate),
    IndBase(Reg),
    IndDispBase(Immediate, Reg),
    IndIdxScale(Reg, i64),
    IndDispIdxScale(Immediate, Reg, i64),
    IndBaseIdxScale(Reg, Reg, i64),
    IndDispBaseIdxScale(Immediate, Reg, Reg, i64),
}

impl Into<Arg> for Reg {
    fn into(self) -> Arg {
        Arg::Register(self)
    }
}

impl Into<Arg> for i64 {
    fn into(self) -> Arg {
        Arg::Immediate(Immediate::Value(self))
    }
}

impl Into<Arg> for &str {
    fn into(self) -> Arg {
        Arg::Immediate(Immediate::Label(self.to_owned()))
    }
}

pub enum AsmLine {
    Label(String),
    Movb(Arg, Arg),
    Movw(Arg, Arg),
    Movl(Arg, Arg),
    Movq(Arg, Arg),
    Movabsq(Arg, Arg),
    Movsbw(Arg, Arg),
    Movsbl(Arg, Arg),
    Movsbq(Arg, Arg),
    Movswl(Arg, Arg),
    Movswq(Arg, Arg),
    Movslq(Arg, Arg),
    Movzbw(Arg, Arg),
    Movzbl(Arg, Arg),
    Movzbq(Arg, Arg),
    Movzwl(Arg, Arg),
    Movzwq(Arg, Arg),
    Leab(Arg, Arg),
    Leaw(Arg, Arg),
    Leal(Arg, Arg),
    Leaq(Arg, Arg),
    Incb(Arg),
    Incw(Arg),
    Incl(Arg),
    Incq(Arg),
    Decb(Arg),
    Decw(Arg),
    Decl(Arg),
    Decq(Arg),
    Negb(Arg),
    Negw(Arg),
    Negl(Arg),
    Negq(Arg),
    Addb(Arg, Arg),
    Addw(Arg, Arg),
    Addl(Arg, Arg),
    Addq(Arg, Arg),
    Subb(Arg, Arg),
    Subw(Arg, Arg),
    Subl(Arg, Arg),
    Subq(Arg, Arg),
    Imulw(Arg, Arg),
    Imull(Arg, Arg),
    Imulq(Arg, Arg),
    Idivl(Arg),
    Cdq,
    Notb(Arg),
    Notw(Arg),
    Notl(Arg),
    Notq(Arg),
    Andb(Arg, Arg),
    Andw(Arg, Arg),
    Andl(Arg, Arg),
    Andq(Arg, Arg),
    Orb(Arg, Arg),
    Orw(Arg, Arg),
    Orl(Arg, Arg),
    Orq(Arg, Arg),
    Xorb(Arg, Arg),
    Xorw(Arg, Arg),
    Xorl(Arg, Arg),
    Xorq(Arg, Arg),
    Shlb(Arg, Arg),
    Shlw(Arg, Arg),
    Shll(Arg, Arg),
    Shlq(Arg, Arg),
    Shrb(Arg, Arg),
    Shrw(Arg, Arg),
    Shrl(Arg, Arg),
    Shrq(Arg, Arg),
    Sarb(Arg, Arg),
    Sarw(Arg, Arg),
    Sarl(Arg, Arg),
    Sarq(Arg, Arg),
    Jmp(String),
    JmpStar(Arg),
    Call(String),
    CallStar(Arg),
    Ret,
    Je(String),
    Jz(String),
    Jne(String),
    Jnz(String),
    Js(String),
    Jns(String),
    Jg(String),
    Jge(String),
    Jl(String),
    Jle(String),
    Ja(String),
    Jae(String),
    Jb(String),
    Jbe(String),
    Cmpb(Arg, Arg),
    Cmpw(Arg, Arg),
    Cmpl(Arg, Arg),
    Cmpq(Arg, Arg),
    Testb(Arg, Arg),
    Testw(Arg, Arg),
    Testl(Arg, Arg),
    Testq(Arg, Arg),
    Sete(Arg),
    Setne(Arg),
    Sets(Arg),
    Setns(Arg),
    Setg(Arg),
    Setge(Arg),
    Setl(Arg),
    Setle(Arg),
    Seta(Arg),
    Setae(Arg),
    Setb(Arg),
    Setbe(Arg),
    Pushq(Arg),
    Popq(Arg),

    String(String),
    Quad(Immediate),

    CallPrintf,
    CallSprintf,
    CallMalloc,
    CallStrlen,
    CallStrcmp,
    CallStrcpy,
    CallStrcat,
}

pub struct AsmBuffer {
    pub lines: Vec<AsmLine>,
}

impl AsmBuffer {
    pub fn new() -> Self {
        Self {
            lines: Vec::new()
        }
    }

    pub fn append(&mut self, mut other: Self) {
        self.lines.append(&mut other.lines);
    }

    pub fn string(&mut self, string: &str) -> &mut Self {
        self.lines.push(AsmLine::String(string.to_owned()));
        self
    }

    pub fn quad(&mut self, immediate: Immediate) -> &mut Self {
        self.lines.push(AsmLine::Quad(immediate));
        self
    }
}

macro_rules! asm_gen_label {
    ($($method:ident, $enum_name:ident),*) => {
        impl AsmBuffer {
            $(pub fn $method(&mut self, label: &str) -> &mut Self {
                self.lines.push(AsmLine::$enum_name(label.to_owned()));
                self
            })*
        }
    };
}

macro_rules! asm_gen_zero_args {
    ($($method:ident, $enum_name:ident),*) => {
        impl AsmBuffer {
            $(pub fn $method(&mut self) -> &mut Self {
                self.lines.push(AsmLine::$enum_name);
                self
            })*
        }
    };
}

macro_rules! asm_gen_one_arg {
    ($($method:ident, $enum_name:ident),*) => {
        impl AsmBuffer {
            $(pub fn $method(&mut self, arg: impl Into<Arg>) -> &mut Self {
                self.lines.push(AsmLine::$enum_name(arg.into()));
                self
            })*
        }
    };
}

macro_rules! asm_gen_two_args {
    ($($method:ident, $enum_name:ident),*) => {
        impl AsmBuffer {
            $(pub fn $method(&mut self, arg1: impl Into<Arg>, arg2: impl Into<Arg>) -> &mut Self {
                self.lines.push(AsmLine::$enum_name(arg1.into(), arg2.into()));
                self
            })*
        }
    };
}

asm_gen_label!(
    label, Label,
    jmp, Jmp,
    call, Call,
    je, Je,
    jz, Jz,
    jne, Jne,
    jnz, Jnz,
    js, Js,
    jns, Jns,
    jg, Jg,
    jge, Jge,
    jl, Jl,
    jle, Jle,
    ja, Ja,
    jae, Jae,
    jb, Jb,
    jbe, Jbe
);

asm_gen_zero_args!(
    cdq, Cdq,
    ret, Ret,
    call_printf, CallPrintf,
    call_sprintf, CallSprintf,
    call_malloc, CallMalloc,
    call_strlen, CallStrlen,
    call_strcmp, CallStrcmp,
    call_strcpy, CallStrcpy,
    call_strcat, CallStrcat
);

asm_gen_one_arg! {
    incb, Incb,
    incw, Incw,
    incl, Incl,
    incq, Incq,
    decb, Decb,
    decw, Decw,
    decl, Decl,
    decq, Decq,
    negb, Negb,
    negw, Negw,
    negl, Negl,
    negq, Negq,
    idivl, Idivl,
    notb, Notb,
    notw, Notw,
    notl, Notl,
    notq, Notq,
    jmp_star, JmpStar,
    call_star, CallStar,
    sete, Sete,
    setne, Setne,
    sets, Sets,
    setns, Setns,
    setg, Setg,
    setge, Setge,
    setl, Setl,
    setle, Setle,
    seta, Seta,
    setae, Setae,
    setb, Setb,
    setbe, Setbe,
    pushq, Pushq,
    popq, Popq
}

asm_gen_two_args!(
    movb, Movb,
    movw, Movw,
    movl, Movl,
    movq, Movq,
    movabsq, Movabsq,
    movsbw, Movsbw,
    movsbl, Movsbl,
    movsbq, Movsbq,
    movswl, Movswl,
    movswq, Movswq,
    movslq, Movslq,
    movzbw, Movzbw,
    movzbl, Movzbl,
    movzbq, Movzbq,
    movzwl, Movzwl,
    movzwq, Movzwq,
    leab, Leab,
    leaw, Leaw,
    leal, Leal,
    leaq, Leaq,
    addb, Addb,
    addw, Addw,
    addl, Addl,
    addq, Addq,
    subb, Subb,
    subw, Subw,
    subl, Subl,
    subq, Subq,
    imulw, Imulw,
    imull, Imull,
    imulq, Imulq,
    andb, Andb,
    andw, Andw,
    andl, Andl,
    andq, Andq,
    orb, Orb,
    orw, Orw,
    orl, Orl,
    orq, Orq,
    xorb, Xorb,
    xorw, Xorw,
    xorl, Xorl,
    xorq, Xorq,
    shlb, Shlb,
    shlw, Shlw,
    shll, Shll,
    shlq, Shlq,
    shrb, Shrb,
    shrw, Shrw,
    shrl, Shrl,
    shrq, Shrq,
    sarb, Sarb,
    sarw, Sarw,
    sarl, Sarl,
    sarq, Sarq,
    cmpb, Cmpb,
    cmpw, Cmpw,
    cmpl, Cmpl,
    cmpq, Cmpq,
    testb, Testb,
    testw, Testw,
    testl, Testl,
    testq, Testq
);

