use std::borrow::Cow;
use std::fmt::Write;
use crate::codegen::assembly::{Arg, AsmBuffer, AsmLine, Immediate, Reg};

fn format_immediate(immediate: &Immediate) -> Cow<str> {
    match immediate {
        Immediate::Value(value) => Cow::Owned(format!("{value}")),
        Immediate::Label(label) => Cow::Borrowed(label),
    }
}

fn format_reg(register: &Reg) -> &'static str {
    match register {
        Reg::Rax => "%rax",
        Reg::Rbx => "%rbx",
        Reg::Rcx => "%rcx",
        Reg::Rdx => "%rdx",
        Reg::Rsi => "%rsi",
        Reg::Rdi => "%rdi",
        Reg::Rbp => "%rbp",
        Reg::Rsp => "%rsp",
        Reg::R8 => "%r8",
        Reg::R9 => "%r9",
        Reg::R10 => "%r10",
        Reg::R11 => "%r11",
        Reg::R12 => "%r12",
        Reg::R13 => "%r13",
        Reg::R14 => "%r14",
        Reg::R15 => "%r15",
        Reg::Eax => "%eax",
        Reg::Ebx => "%ebx",
        Reg::Ecx => "%ecx",
        Reg::Edx => "%edx",
        Reg::Esi => "%esi",
        Reg::Edi => "%edi",
        Reg::Ebp => "%ebp",
        Reg::Esp => "%esp",
        Reg::R8d => "%r8d",
        Reg::R9d => "%r9d",
        Reg::R10d => "%r10d",
        Reg::R11d => "%r11d",
        Reg::R12d => "%r12d",
        Reg::R13d => "%r13d",
        Reg::R14d => "%r14d",
        Reg::R15d => "%r15d",
        Reg::Ax => "%ax",
        Reg::Bx => "%bx",
        Reg::Cx => "%cx",
        Reg::Dx => "%dx",
        Reg::Si => "%si",
        Reg::Di => "%di",
        Reg::Bp => "%bp",
        Reg::Sp => "%sp",
        Reg::R8w => "%r8w",
        Reg::R9w => "%r9w",
        Reg::R10w => "%r10w",
        Reg::R11w => "%r11w",
        Reg::R12w => "%r12w",
        Reg::R13w => "%r13w",
        Reg::R14w => "%r14w",
        Reg::R15w => "%r15w",
        Reg::Al => "%al",
        Reg::Bl => "%bl",
        Reg::Cl => "%cl",
        Reg::Dl => "%dl",
        Reg::Ah => "%ah",
        Reg::Bh => "%bh",
        Reg::Ch => "%ch",
        Reg::Dh => "%dh",
        Reg::Sil => "%sil",
        Reg::Dil => "%dil",
        Reg::Bpl => "%bpl",
        Reg::Spl => "%spl",
        Reg::R8b => "%r8b",
        Reg::R9b => "%r9b",
        Reg::R10b => "%r10b",
        Reg::R11b => "%r11b",
        Reg::R12b => "%r12b",
        Reg::R13b => "%r13b",
        Reg::R14b => "%r14b",
        Reg::R15b => "%r15b",
    }
}

fn format_arg(arg: &Arg) -> Cow<str> {
    match arg {
        Arg::Register(register) => {
            Cow::Borrowed(format_reg(register))
        }
        Arg::Immediate(immediate) => {
            Cow::Owned(format!("${}", format_immediate(immediate)))
        }
        Arg::IndBase(base) => {
            Cow::Owned(format!("({})", format_reg(base)))
        }
        Arg::IndDispBase(displacement, base) => {
            Cow::Owned(format!("{}({})", format_immediate(displacement), format_reg(base)))
        }
        Arg::IndIdxScale(index, scale) => {
            Cow::Owned(format!("(, {}, {scale})", format_reg(index)))
        }
        Arg::IndDispIdxScale(displacement, index, scale) => {
            Cow::Owned(format!("{}(, {}, {scale})", format_immediate(displacement), format_reg(index)))
        }
        Arg::IndBaseIdxScale(base, index, scale) => {
            Cow::Owned(format!("({}, {}, {scale})", format_reg(base), format_reg(index)))
        }
        Arg::IndDispBaseIdxScale(displacement, base, index, scale) => {
            Cow::Owned(format!("{}({}, {}, {scale})", format_immediate(displacement), format_reg(base), format_reg(index)))
        }
    }
}

fn fmt_label(op: &str, label: &str) -> Cow<'static, str> {
    Cow::Owned(format!("{op} {label}\n"))
}

fn fmt_one_arg(op: &str, arg: &Arg) -> Cow<'static, str> {
    Cow::Owned(format!("{} {}\n", op, format_arg(arg)))
}

fn fmt_two_args(op: &str, arg1: &Arg, arg2: &Arg) -> Cow<'static, str> {
    Cow::Owned(format!("{} {}, {}\n", op, format_arg(arg1), format_arg(arg2)))
}

pub fn gas_format_asm(buffer: &AsmBuffer) -> String {
    let mut output = String::from("\
        .text\n\
        .globl main\n\
    ");

    for line in buffer.lines.iter() {
        output.push_str(&match line {
            AsmLine::Label(label) => Cow::Owned(format!("{label}:\n")),
            AsmLine::Movb(a, b) => fmt_two_args("movb", a, b),
            AsmLine::Movw(a, b) => fmt_two_args("movw", a, b),
            AsmLine::Movl(a, b) => fmt_two_args("movl", a, b),
            AsmLine::Movq(a, b) => fmt_two_args("movq", a, b),
            AsmLine::Movabsq(a, b) => fmt_two_args("movabsq", a, b),
            AsmLine::Movsbw(a, b) => fmt_two_args("movsbw", a, b),
            AsmLine::Movsbl(a, b) => fmt_two_args("movsbl", a, b),
            AsmLine::Movsbq(a, b) => fmt_two_args("movsbq", a, b),
            AsmLine::Movswl(a, b) => fmt_two_args("movswl", a, b),
            AsmLine::Movswq(a, b) => fmt_two_args("movswq", a, b),
            AsmLine::Movslq(a, b) => fmt_two_args("movslq", a, b),
            AsmLine::Movzbw(a, b) => fmt_two_args("movzbw", a, b),
            AsmLine::Movzbl(a, b) => fmt_two_args("movzbl", a, b),
            AsmLine::Movzbq(a, b) => fmt_two_args("movzbq", a, b),
            AsmLine::Movzwl(a, b) => fmt_two_args("movzwl", a, b),
            AsmLine::Movzwq(a, b) => fmt_two_args("movzwq", a, b),
            AsmLine::Leab(a, b) => fmt_two_args("leab", a, b),
            AsmLine::Leaw(a, b) => fmt_two_args("leaw", a, b),
            AsmLine::Leal(a, b) => fmt_two_args("leal", a, b),
            AsmLine::Leaq(a, b) => fmt_two_args("leaq", a, b),
            AsmLine::Incb(a) => fmt_one_arg("incb", a),
            AsmLine::Incw(a) => fmt_one_arg("incw", a),
            AsmLine::Incl(a) => fmt_one_arg("incl", a),
            AsmLine::Incq(a) => fmt_one_arg("incq", a),
            AsmLine::Decb(a) => fmt_one_arg("decb", a),
            AsmLine::Decw(a) => fmt_one_arg("decw", a),
            AsmLine::Decl(a) => fmt_one_arg("decl", a),
            AsmLine::Decq(a) => fmt_one_arg("decq", a),
            AsmLine::Negb(a) => fmt_one_arg("negb", a),
            AsmLine::Negw(a) => fmt_one_arg("negw", a),
            AsmLine::Negl(a) => fmt_one_arg("negl", a),
            AsmLine::Negq(a) => fmt_one_arg("negq", a),
            AsmLine::Addb(a, b) => fmt_two_args("addb", a, b),
            AsmLine::Addw(a, b) => fmt_two_args("addw", a, b),
            AsmLine::Addl(a, b) => fmt_two_args("addl", a, b),
            AsmLine::Addq(a, b) => fmt_two_args("addq", a, b),
            AsmLine::Subb(a, b) => fmt_two_args("subb", a, b),
            AsmLine::Subw(a, b) => fmt_two_args("subw", a, b),
            AsmLine::Subl(a, b) => fmt_two_args("subl", a, b),
            AsmLine::Subq(a, b) => fmt_two_args("subq", a, b),
            AsmLine::Imulw(a, b) => fmt_two_args("imulw", a, b),
            AsmLine::Imull(a, b) => fmt_two_args("imull", a, b),
            AsmLine::Imulq(a, b) => fmt_two_args("imulq", a, b),
            AsmLine::Idivl(a) => fmt_one_arg("idivl", a),
            AsmLine::Cdq => Cow::Borrowed("cdq\n"),
            AsmLine::Notb(a) => fmt_one_arg("notb", a),
            AsmLine::Notw(a) => fmt_one_arg("notw", a),
            AsmLine::Notl(a) => fmt_one_arg("notl", a),
            AsmLine::Notq(a) => fmt_one_arg("notq", a),
            AsmLine::Andb(a, b) => fmt_two_args("andb", a, b),
            AsmLine::Andw(a, b) => fmt_two_args("andw", a, b),
            AsmLine::Andl(a, b) => fmt_two_args("andl", a, b),
            AsmLine::Andq(a, b) => fmt_two_args("andq", a, b),
            AsmLine::Orb(a, b) => fmt_two_args("orb", a, b),
            AsmLine::Orw(a, b) => fmt_two_args("orw", a, b),
            AsmLine::Orl(a, b) => fmt_two_args("orl", a, b),
            AsmLine::Orq(a, b) => fmt_two_args("orq", a, b),
            AsmLine::Xorb(a, b) => fmt_two_args("xorb", a, b),
            AsmLine::Xorw(a, b) => fmt_two_args("xorw", a, b),
            AsmLine::Xorl(a, b) => fmt_two_args("xorl", a, b),
            AsmLine::Xorq(a, b) => fmt_two_args("xorq", a, b),
            AsmLine::Shlb(a, b) => fmt_two_args("shlb", a, b),
            AsmLine::Shlw(a, b) => fmt_two_args("shlw", a, b),
            AsmLine::Shll(a, b) => fmt_two_args("shll", a, b),
            AsmLine::Shlq(a, b) => fmt_two_args("shlq", a, b),
            AsmLine::Shrb(a, b) => fmt_two_args("shrb", a, b),
            AsmLine::Shrw(a, b) => fmt_two_args("shrw", a, b),
            AsmLine::Shrl(a, b) => fmt_two_args("shrl", a, b),
            AsmLine::Shrq(a, b) => fmt_two_args("shrq", a, b),
            AsmLine::Sarb(a, b) => fmt_two_args("sarb", a, b),
            AsmLine::Sarw(a, b) => fmt_two_args("sarw", a, b),
            AsmLine::Sarl(a, b) => fmt_two_args("sarl", a, b),
            AsmLine::Sarq(a, b) => fmt_two_args("sarq", a, b),
            AsmLine::Jmp(l) => fmt_label("jmp", l),
            AsmLine::JmpStar(a) => Cow::Owned(format!("jmp *{}\n", format_arg(a))),
            AsmLine::Call(l) => fmt_label("call", l),
            AsmLine::CallStar(a) => Cow::Owned(format!("call *{}\n", format_arg(a))),
            AsmLine::Ret => Cow::Borrowed("ret\n"),
            AsmLine::Je(l) => fmt_label("je", l),
            AsmLine::Jz(l) => fmt_label("jz", l),
            AsmLine::Jne(l) => fmt_label("jne", l),
            AsmLine::Jnz(l) => fmt_label("jnz", l),
            AsmLine::Js(l) => fmt_label("js", l),
            AsmLine::Jns(l) => fmt_label("jns", l),
            AsmLine::Jg(l) => fmt_label("jg", l),
            AsmLine::Jge(l) => fmt_label("jge", l),
            AsmLine::Jl(l) => fmt_label("jl", l),
            AsmLine::Jle(l) => fmt_label("jle", l),
            AsmLine::Ja(l) => fmt_label("ja", l),
            AsmLine::Jae(l) => fmt_label("jae", l),
            AsmLine::Jb(l) => fmt_label("jb", l),
            AsmLine::Jbe(l) => fmt_label("jbe", l),
            AsmLine::Cmpb(a, b) => fmt_two_args("cmpb", a, b),
            AsmLine::Cmpw(a, b) => fmt_two_args("cmpw", a, b),
            AsmLine::Cmpl(a, b) => fmt_two_args("cmpl", a, b),
            AsmLine::Cmpq(a, b) => fmt_two_args("cmpq", a, b),
            AsmLine::Testb(a, b) => fmt_two_args("testb", a, b),
            AsmLine::Testw(a, b) => fmt_two_args("testw", a, b),
            AsmLine::Testl(a, b) => fmt_two_args("testl", a, b),
            AsmLine::Testq(a, b) => fmt_two_args("testq", a, b),
            AsmLine::Sete(a) => fmt_one_arg("sete", a),
            AsmLine::Setne(a) => fmt_one_arg("setne", a),
            AsmLine::Sets(a) => fmt_one_arg("sets", a),
            AsmLine::Setns(a) => fmt_one_arg("setns", a),
            AsmLine::Setg(a) => fmt_one_arg("setg", a),
            AsmLine::Setge(a) => fmt_one_arg("setge", a),
            AsmLine::Setl(a) => fmt_one_arg("setl", a),
            AsmLine::Setle(a) => fmt_one_arg("setle", a),
            AsmLine::Seta(a) => fmt_one_arg("seta", a),
            AsmLine::Setae(a) => fmt_one_arg("setae", a),
            AsmLine::Setb(a) => fmt_one_arg("setb", a),
            AsmLine::Setbe(a) => fmt_one_arg("setbe", a),
            AsmLine::Pushq(a) => fmt_one_arg("pushq", a),
            AsmLine::Popq(a) => fmt_one_arg("popq", a),
            AsmLine::String(string) => {
                let mut output = String::from(".byte ");
                for byte in string.as_bytes() {
                    write!(&mut output, "{:#04x}, ", *byte).unwrap();
                }
                write!(&mut output, "0x00\n").unwrap();
                Cow::Owned(output)
            }
            AsmLine::Quad(immediate) => {
                Cow::Owned(format!(".quad {}\n", format_immediate(immediate)))
            }
            AsmLine::CallPrintf => Cow::Borrowed("callq printf\n"),
            AsmLine::CallSprintf => Cow::Borrowed("callq sprintf\n"),
            AsmLine::CallMalloc => Cow::Borrowed("callq malloc\n"),
            AsmLine::CallStrlen => Cow::Borrowed("callq strlen\n"),
            AsmLine::CallStrcmp => Cow::Borrowed("callq strcmp\n"),
            AsmLine::CallStrcpy => Cow::Borrowed("callq strcpy\n"),
            AsmLine::CallStrcat => Cow::Borrowed("callq strcat\n"),
        })
    }

    output
}