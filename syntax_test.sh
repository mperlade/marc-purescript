tested=0
passed=0

function syntax_test_dir() {
  for file in "$1"/*.purs
  do
    ./ppurs --parse-only "$file"
    exit_code=$?
    ((tested = tested + 1))
    if [ $exit_code -eq $2 ]
    then
      ((passed = passed + 1))
      echo "$file: test passed."
    else
      echo "$file: test failed."
    fi
  done
}

syntax_test_dir "./tests-10-nov/syntax/bad" 1
syntax_test_dir "./tests-10-nov/syntax/good" 0
syntax_test_dir "./tests-10-nov/typing/bad" 0
syntax_test_dir "./tests-10-nov/typing/good" 0
syntax_test_dir "./tests-10-nov/exec" 0

echo "$tested files tested, $passed passed."