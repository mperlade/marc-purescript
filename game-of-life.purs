module Main where

import Prelude
import Effect
import Effect.Console

fail::forall a.a -> a
fail x = let _ = 1 / 0 in x --A very professional way of handling exceptions

data List a = Cons a (List a) | Nil
data Couple a b = Couple a b
data Int3 = T Int Int Int

rule::Int3 -> Int3 -> Int3 -> Int
rule left middle right = case left of
	T c0 c1 c2 -> case middle of
		T c3 c4 c5 -> case right of
			T c6 c7 c8 -> let neighbour_sum = c0 + c1 + c2 + c3 + c5 + c6 + c7 + c8 in
				if c4 == 0 then
					if neighbour_sum == 3 then 1 else 0
				else
					if neighbour_sum == 2 || neighbour_sum == 3 then 1 else 0

update_lin_rec::Int3 -> Int3 -> Int3 -> List Int -> List Int -> List Int -> Couple Int3 (List Int)
update_lin_rec first prev current top middle bottom = case top of
	Nil -> let new = rule prev current first in Couple current (Cons new Nil)
	Cons h_t t_t -> case middle of
		Cons h_m t_m -> case bottom of
			Cons h_b t_b -> let
					next = T h_t h_m h_b
					new = rule prev current next
					rec_out = update_lin_rec first current next t_t t_m t_b
				in case rec_out of
					Couple last list -> Couple last (Cons new list)
			_ -> fail (Couple (T 0 0 0) Nil)
		_ -> fail (Couple (T 0 0 0) Nil)

update_lin::List Int -> List Int -> List Int -> List Int
update_lin t m b = case t of
	Cons first_t (Cons second_t t_t) -> case m of
		Cons first_m (Cons second_m t_m) -> case b of
			Cons first_b (Cons second_b t_b) -> let
					first = T first_t first_m first_b
					second = T second_t second_m second_b
					rec_out = update_lin_rec first first second t_t t_m t_b
				in case rec_out of
					Couple last list -> let new_first = rule last first second in Cons new_first list
			_ -> fail Nil
		_ -> fail Nil
	_ -> fail Nil

update_grid_rec::List Int -> List Int -> List Int -> List (List Int) -> Couple (List Int) (List (List Int))
update_grid_rec first prev current Nil = Couple current (Cons (update_lin prev current first) Nil)
update_grid_rec first prev current (Cons head tail) = let
		new = update_lin prev current head
		rec_out = update_grid_rec first current head tail
	in case rec_out of
		Couple last list -> Couple last (Cons new list)

update_grid::List (List Int) -> List (List Int)
update_grid (Cons first (Cons second tail)) = let
		rec_out = update_grid_rec first first second tail
	in case rec_out of
		Couple last list -> let new_first = update_lin last first second in Cons new_first list
update_grid _ = fail Nil

instance Show (List Int) where
	show Nil = ""
	show (Cons head tail) = (if head /= 0 then "██" else "  ") <> show tail

display_grid::List (List Int) -> Effect Unit
display_grid Nil = pure unit
display_grid (Cons head tail) = do
	log (show head)
	display_grid tail

initialize::Int -> Int -> Int
initialize i j = case i of
	0 -> if j == 1 then 1 else 0
	1 -> if j == 2 then 1 else 0
	2 -> if j <= 2 then 1 else 0
	_ -> 0

create_list_rec::Int -> Int -> Int -> Int -> List Int
create_list_rec n m i j = if j == m then
		Nil
	else
		Cons (initialize i j) (create_list_rec n m i (j + 1))

create_grid_rec::Int -> Int -> Int -> List (List Int)
create_grid_rec n m i = if i == n then
		Nil
	else
		Cons (create_list_rec n m i 0) (create_grid_rec n m (i + 1))

create_grid::Int -> Int -> List (List Int)
create_grid n m = create_grid_rec n m 0

loop::Int -> Int -> List (List Int) -> Effect Unit
loop n max grid = if n > max then pure unit
	else do
		log ("Generation " <> (show n))
		display_grid grid
		let new_grid = update_grid grid in loop (n + 1) max new_grid

main::Effect Unit
main = loop 1 50 (create_grid 10 10)
