file="$1"
./ppurs "$file"
exit_code=$?
base_name=$(basename -- "$file")
assembly_name="${base_name%.purs}.s"
if [ $exit_code -eq 0 ]
then
  gcc -g -no-pie "$assembly_name"
  ./a.out
  rm "./a.out"
  rm "$assembly_name"
fi