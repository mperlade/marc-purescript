build:
	cargo build --release
	cp ./target/release/ppurs ./

clean:
	cargo clean
	rm ./ppurs